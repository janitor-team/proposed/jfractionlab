You have downloaded the sourcecode of JFractionLab!

This is a description how you can compile the program on a unixlike system.
On windows you can read mkJflJar.sh and run the analogous commands.

1.	Install a JDK!

2.	Unzip the source!

3.	Go to your homedirectory

4. 	Copy the script mkJFLPack.sh to your homedirectory and run it like:
	./mkJFLPack.sh <versionNB> <path to the src>
   	./mkJFLPack.sh 083 foo/jflsrc_083

