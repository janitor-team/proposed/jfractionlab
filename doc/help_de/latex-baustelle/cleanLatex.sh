#!/bin/sh

if [ $# -ne 1 ]; then
	echo "Bitte Dateinamen ohne Endung angeben!"
else
	rm "$1".aux "$1".log "$1".dvi "$1".pdf *~
fi
