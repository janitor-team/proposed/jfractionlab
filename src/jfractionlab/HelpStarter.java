/**
 *	JFractionLab
 *	Copyright (C) 2005 jochen georges, gnugeo _ at _ gnugeo _ dot _ de
 *
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/

package jfractionlab;

/**
 * It would be much nicer to use the standard desktop browser,
 * this should be possible with java.awt.Desktop
 * I tried to do so, but it does not work yet.
 * I do not know how to set an URI to the local resource.
 * At the Moment this class is simply a pipe to the class HelpBrowser
 * 
 * @author jochen
 *
 */
public class HelpStarter{
	
	public HelpStarter(String lang, String startpoint){
//		System.out.println("Helpstarter "+lang+", "+startpoint);
		new HelpBrowser(lang, startpoint);
//		startDesktopBrowser(lang, startpoint);
	}
	
//	public void startDesktopBrowser(String lang, String startpoint){
//		Desktop desktop;
//		if (Desktop.isDesktopSupported()) {
//			desktop = Desktop.getDesktop();
//			if (desktop.isSupported(Desktop.Action.BROWSE)) {
//				try {
//					URI uri = null;
////					that works fine!
////						uri = new URI("http://www.gnugeo.de");
////					that does notwork :-(
//						File file = new File(getClass().getResource("/index.html").toURI());
//						uri = file.toURI();
////					that does not work :-(
////						uri = getClass().getResource("/index.html").toURI();
//					JOptionPane.showMessageDialog(
//							null,
//							"URI = "+uri.toString()
//						);
//					desktop.browse(uri);
//				}catch(IOException ioe) {
//					ioe.printStackTrace();
//					new HelpBrowser(lang, startpoint);
//				}
//				catch(URISyntaxException use) {
//					use.printStackTrace();
//					new HelpBrowser(lang, startpoint);
//				}
//			}
//		}
//	}
}