/**
 *	Copyright (C) 2005 jochen georges, gnugeo _ at _ gnugeo _ dot _ de
 *
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/

package jfractionlab;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;


public class ConfManager{
	
	private ConfSettings cs;
	private String confdir;
	private String conffile = "jfractionlab.conf";
	private String confpath;
	
	
	public ConfManager(){
		if (JFractionLab.os.contains("win")){
			confdir = System.getProperty("user.home") +
					System.getProperty("file.separator")+"jfractionlab";
		}else{
			confdir = System.getProperty("user.home") +
					System.getProperty("file.separator")+".jfractionlab";
		}
		confpath = confdir+
				System.getProperty("file.separator")+
				conffile;
		
		if(confFileExists()){
			//System.out.println("if conffileexists");
			if(!readConfFile()){
				//System.out.println("if!readConfFile");
				createDefaultConfFile();
			}
			else{ //configurations should be ok
				//alertConfigurations();
			}
		}else{
			createDefaultConfFile();
		}
	}
	private boolean readConfFile(){
		try{
			FileInputStream fs = new FileInputStream(confpath);
			ObjectInputStream is = new ObjectInputStream(fs);
			cs = (ConfSettings)is.readObject();
			is.close();
			//System.out.println("readConffile true");
			return true;
		}catch( Exception ex ) {
			//System.out.println("readConffile false");
			return false;
		}
	}
	
	private void createDefaultConfFile(){
		cs = new ConfSettings();
		writeConfFile();
	}
	
	private void writeConfFile(){
		boolean isOK = true;
		if(!(new File(confdir).exists())){
			if(!(new File(confdir).mkdirs())){
				isOK = false;
			}
		}
		if(isOK){
			File file = new File(confpath);
			try{
				FileOutputStream fs = new FileOutputStream(file);
				ObjectOutputStream os = new ObjectOutputStream(fs);
				os.writeObject(cs);
				os.close();
				//folgende zeilen verhindern einen erneuten schreibenden zugriff auf di datei!!?? 
	//				String[] str_ar_command = {
	//						"ATTRIB",
	//						"+h",
	//						confpath
	//				};
	//				try {
	//					Runtime.getRuntime().exec(str_ar_command);
	//				} catch (IOException e) {
	//					System.out.println("ERROR : cmd.exe attrib +h");
	//					e.printStackTrace();
	//				}
			}catch (IOException ex) {
				ex.printStackTrace();
				//FIXME joptionpane with nice text
				System.out.println("ConfManager: writeConfFile does not work");
			}//catch
		}else{
			//FIXME joptionpane with nice text
			System.out.println("ConfManager: writeConfFile does not work, mkdirs()-Error");
		}
	}
	
	private boolean confFileExists(){
		if(new File(confpath).exists()){
			//System.out.println("confFileExists() true");
			return true;
		}else{
			return false;
		}
	}

	public boolean isTipAtStart() {
		return cs.isTipAtStart();
	}
	public void setTipAtStart(boolean bl){
		cs.setTipAtStart(bl);
		writeConfFile();
	}
		
	public String[] getUnoConfJars(){
		return cs.getUnoConfJars();
	}
	public void setUnoConfJars(String[] str){
		cs.setUnoConfJars(str);
		writeConfFile();
	}

	public String getConfdir(){
		return confdir;
	}
	public String getConffile(){
		return conffile;
	}
	public String getConfpath(){
		return confpath;
	}
	public String alertConfigurations(){
		return cs.toString();
	}
}