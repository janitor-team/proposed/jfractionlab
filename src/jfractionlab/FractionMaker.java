/**
 *	JFractionLab
 *	Copyright (C) 2005 jochen georges, gnugeo _ at _ gnugeo _ dot _ de
 *
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/

package jfractionlab;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class FractionMaker{

	private int numerator_1;
	private int denominator_1;
	private int numerator_2;
	private int denominator_2;
	
	/**
	 * 
	 *
	 */
	public FractionMaker(){}
	
	/**
	 * 
	 * @param max
	 */
	public FractionMaker(int max){
		mkTwoFractions(max);
	}
	
	/**
	 * 
	 * @param numerator_1
	 * @param denominator_1
	 * @param numerator_2
	 * @param denominator_2
	 */
	public FractionMaker(int numerator_1, int denominator_1,int numerator_2, int denominator_2){
		this.numerator_1 = numerator_1;
		this.denominator_1 = denominator_1;
		this.numerator_2 = numerator_2;
		this.denominator_2 = denominator_2;
	}
	
	/**
	 * macht einen unechten bruch kleiner als max
	 */
	public void mkImproperFraction(){
		int max = 7;
//		for (int pp = 0; pp < 10; pp++){
			do{
				numerator_1 = (Math.abs(JFractionLab.ran.nextInt())%80)+1;
				denominator_1 = (Math.abs(JFractionLab.ran.nextInt())%10)+1;
			}while ((numerator_1 <= denominator_1) || (numerator_1/denominator_1>max));
//		}
	}
	
	public void mkImproperFraction(int max){
//		for (int pp = 0; pp < 10; pp++){
			do{
				numerator_1 = (Math.abs(JFractionLab.ran.nextInt())%max)+1;
				denominator_1 = (Math.abs(JFractionLab.ran.nextInt())%max)+1;
			}while ((numerator_1 <= denominator_1));
//		}
	}
	
	/**
	 * 
	 * @param max
	 */
	public void mkTwoFractions(int max){
		do{
			numerator_1 = (Math.abs(JFractionLab.ran.nextInt())%max)+1;
			//r.nextInt() creates a rindom number (int)
			//Math.abs() makes the number positive
			//%x sets the exclusive maximum
			//+1 avoids 0
			denominator_1 = (Math.abs(JFractionLab.ran.nextInt())%max)+1;
		}while (numerator_1 >= denominator_1);
		do{
			numerator_2 = (Math.abs(JFractionLab.ran.nextInt())%max)+1;
			denominator_2 = (Math.abs(JFractionLab.ran.nextInt())%max)+1;
		}while (numerator_2 >= denominator_2);
	}
	
	/**
	 * // optionen speicherbar machen (kuerzen, eigene aufgaben)
	 * @param max
	 */
	public void mkOneFraction(int max){
		do{
			numerator_1 = (Math.abs(JFractionLab.ran.nextInt())%max)+1;
			denominator_1 = (Math.abs(JFractionLab.ran.nextInt())%max)+1;
		}while (numerator_1 >= denominator_1);
	}
	
	public void mkOneReducedFraction(int max){
		do{
			mkOneFraction(max);
		}while (JFractionLab.greatestCommonDivisor(numerator_1, denominator_1)!=1);
	}
	
	/**
	 * 
	 * @param max
	 */
	public void mkTwoFractionsWithEqualNumerators(int max){ //gleiche Zaehler
		//makes two fractions with equal numerators
		do{
			numerator_1 = (Math.abs(JFractionLab.ran.nextInt())%max)+1;
			numerator_2 = numerator_1;
			denominator_1 = (Math.abs(JFractionLab.ran.nextInt())%max)+1;
			denominator_2 = (Math.abs(JFractionLab.ran.nextInt())%max)+1;
		}while (numerator_1 > denominator_1 || numerator_2 > denominator_2);
		//System.out.println("gleiche numerator: "
		//		+String.valueOf(numerator_1)+"/"+String.valueOf(denominator_1)+" ; "
		//		+String.valueOf(numerator_2)+"/"+String.valueOf(denominator_2)
		//);
	}	
	
	/**
	 * 
	 * @param max
	 */
	public void mkTwoFractionsWithEqualDenominators(int max){ //gleiche Nenner
		//makes two fractions with equal denominators
		do{
			numerator_1 = (Math.abs(JFractionLab.ran.nextInt())%max)+1;
			numerator_2 = (Math.abs(JFractionLab.ran.nextInt())%max)+1;
			denominator_1 = (Math.abs(JFractionLab.ran.nextInt())%max)+1;
			denominator_2 = denominator_1;
		}while (numerator_1 > denominator_1 || numerator_2 > denominator_2);
	}
	
	public double mkDecimalFraction(boolean easy){
		if(easy){
			int[] x = {2, 4, 5, 10, 20, 25, 50};
			int ran = (Math.abs(JFractionLab.ran.nextInt())%7);
			denominator_1 = x[ran];
		}else{
			denominator_1 = (Math.abs(JFractionLab.ran.nextInt())%99);
		}
		if(denominator_1 == 2){
			numerator_1 = 1;
		}else if(denominator_1 == 4){
			do{
				//0<numerator_1<4
				numerator_1 = (Math.abs(JFractionLab.ran.nextInt())%3+1);
			}while(numerator_1 >= denominator_1);
		}else if(denominator_1 == 5){
			do{
				//0<numerator_1<5
				numerator_1 = (Math.abs(JFractionLab.ran.nextInt())%4+1);
			}while(numerator_1 >= denominator_1);
		}else if(denominator_1 == 10){
			do{
				//0<numerator_1<10
				numerator_1 = (Math.abs(JFractionLab.ran.nextInt())%10+1);
			}while(numerator_1 >= denominator_1);
		}else{
			do{
				//0<numerator_1<49
				numerator_1 = (Math.abs(JFractionLab.ran.nextInt())%49+1);
			}while(numerator_1 >= denominator_1);
		}
		BigDecimal bn = new BigDecimal(numerator_1);
		BigDecimal bd = new BigDecimal(denominator_1);
		return bn.divide(bd).setScale(2, RoundingMode.HALF_EVEN).doubleValue();
	}
	
	/**
	 * 
	 * @param numerator
	 */
	public void setZaehler_1(int numerator){
		this.numerator_1 = numerator;
	}
	
	/**
	 * 
	 * @param denominator
	 */
	public void setNenner_1(int denominator){
		this.denominator_1 = denominator;
	}
	
	/**
	 * 
	 * @param numerator
	 */
	public void setZaehler_2(int numerator){
		this.numerator_2 = numerator;
	}
	
	/**
	 * 
	 * @param denominator
	 */
	public void setNenner_2(int denominator){
		this.denominator_2 = denominator;
	}
	
	/**
	 * 
	 * @return
	 */
	public int getNumerator_1(){
		return(numerator_1);
	}
	
	/**
	 * 
	 * @return
	 */
	public int getNumerator_2(){
		return(numerator_2);
	}
	
	/**
	 * 
	 * @return
	 */
	public int getDenominator_1(){
		return(denominator_1);
	}
	
	/**
	 * 
	 * @return
	 */
	public int getDenominator_2(){
		return(denominator_2);
	}
}//class