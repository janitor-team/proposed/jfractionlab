/**
 *	JFractionLab
 *	Copyright (C) 2005 jochen georges, gnugeo _ at _ gnugeo _ dot _ de
 *
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/

/**
 * tipp hinzufuegen
 * der tipp zeigt die teiler, so dass man den gemeinsamen teiler suchen kann
 */

package jfractionlab.exerciseDialogs;

import info.clearthought.layout.TableLayout;

import java.awt.Color;
import java.awt.Container;
import java.awt.HeadlessException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;


import javax.swing.JButton;
import javax.swing.JLabel;

import jfractionlab.FractionMaker;
import jfractionlab.HelpStarter;
import jfractionlab.JFractionLab;
import jfractionlab.MyJTextField;
import jfractionlab.displays.FractionAsCircle;
import jfractionlab.displays.FractionLine;
import jfractionlab.exerciseGenerator.ExerciseGenerator;
import jfractionlab.jflDialogs.ReduceHintDialog;
import jfractionlab.jflDialogs.UsabilityDialog;
import jfractionlab.jflDialogs.WorkSheetDialog;


public class ReduceFraction extends ExerciseDialog implements ActionListener, KeyListener{
	static final long serialVersionUID = JFractionLab.serialVersionUID;
	//GUI
	private JLabel lb_instruction = new JLabel("", JLabel.CENTER);
	private MyJTextField tf_numerator_1 = new MyJTextField(2);
	private MyJTextField tf_denominator_1 = new MyJTextField(2);
	public MyJTextField tf_numerator_2 = new MyJTextField(2);
	public MyJTextField tf_denominator_2 = new MyJTextField(2);
	private FractionAsCircle pizza_1 = new FractionAsCircle();
	private FractionAsCircle pizza_2 = new FractionAsCircle();
	private JLabel lb_equal = new JLabel("", JLabel.CENTER);
	private JButton btn_tipp; 
	//Zahlen
	private FractionMaker fraction;
	private int numerator_1;
	private int denominator_1;
	//private int numerator_2;
	//private int denominator_2;
	private int reducefactor;
	private boolean tipIsSet = false;
	/**
	 * 
	 * @param owner
	 * @param lx
	 * @param ly
	 * @param sx
	 * @param sy
	 * @throws HeadlessException
	 */
	public ReduceFraction(JFractionLab owner, int lx, int ly, int sx, int sy) throws HeadlessException {
		super(lx,ly,sx,sy);
		setTitle(lang.Messages.getString("reduce_the_fraction"));
		btn_continue = new JButton(lang.Messages.getString("continue"));
		btn_tipp = new JButton(lang.Messages.getString("Tipp"));
		btn_end = new JButton(lang.Messages.getString("end"));
		this.owner = owner;
		
		rb_random.addActionListener(this);
		jmOptions.add(rb_random);
		rb_custom.addActionListener(this);
		jmOptions.add(rb_custom);
		jmb.add(jmOptions);
		jmiCreateWorkSheet.addActionListener(this);
		jmWorkSheet.add(jmiCreateWorkSheet);
		jmb.add(jmWorkSheet);
		jmiHelp.addActionListener(this);
		jmb.add(jmHelp);
		setJMenuBar(jmb);

		double sizes[][] = {{	
			//Spalten
			5,
			TableLayout.FILL,0.06,TableLayout.FILL,
			10,10,
			TableLayout.FILL,0.06,TableLayout.FILL,
			0.15,
			5
		}, {
			//Zeilen
			5,
			TableLayout.FILL,	//label
			TableLayout.FILL,	//nominator / btn
			12,					// -----
			TableLayout.FILL,	//denominator / btn
			12,
			TableLayout.FILL,	//btn
			0.5,
			TableLayout.FILL,
			5
		}};
		Container content = getContentPane();
        	content.setLayout(new TableLayout(sizes));
		content.setBackground(Color.white);
		
			lb_instruction.setFont(JFractionLab.infofont);
		content.add(lb_instruction, "0,1,8,0");
			addTextField(tf_numerator_1);
			tf_numerator_1.addKeyListener(this);
		content.add(tf_numerator_1, "2,2,c,b");
		FractionLine bs_1 = new FractionLine();
		content.add(bs_1, "2,3");
			addTextField(tf_denominator_1);
			tf_denominator_1.addKeyListener(this);
		content.add(tf_denominator_1, "2,4,c,t");
		content.add(pizza_1, "1,5,4,7");
			lb_equal.setFont(JFractionLab.infofont);
			lb_equal.setText("=");
		content.add(lb_equal, "4,3,5,3,f,c");
			addTextField(tf_numerator_2);
			tf_numerator_2.addKeyListener(this);
		content.add(tf_numerator_2, "7,2,c,b");
		FractionLine bs_2 = new FractionLine();
		content.add(bs_2, "7,3");
			addTextField(tf_denominator_2);
			tf_denominator_2.addKeyListener(this);
		content.add(tf_denominator_2, "7,4,c,t");
		content.add(pizza_2, "5,5,8,7");
			btn_continue.addKeyListener(this);
			btn_continue.addActionListener(this);
		content.add(btn_continue, "9,2,f,c");
			btn_tipp.addActionListener(this);
		content.add(btn_tipp, "9,4,f,c");
			btn_end.addActionListener(this);
		content.add(btn_end, "9,6,f,c");
			points = owner.points_reduceFraction;
			//System.out.println("points = " + String.valueOf(points));
			//pdsp.setBackground(Color.red);
			pdsp.setText(String.valueOf(points));
		content.add(pdsp, "9,7");
		//content.add(pdsp, "9,5,f,b"); //Warum geht das nicht??????
			lb_info.setFont(JFractionLab.infofont);
			lb_info.setText(lang.Messages.getString("you_just_need_nb_and_enter"));
		content.add(lb_info, "0,8,8,8");
		makeProblem();
		String[] ar_howto = {"howto_nb_and_enter", "howto_show_denominators"};
		new UsabilityDialog(ar_howto);//new UsabilityDialog
	}//Konstruktor
	
	/**
	 * 
	 *
	 */
	protected void makeProblem(){
		//System.out.println("makeProblem");
		int factor;
		factor = (Math.abs(JFractionLab.ran.nextInt())%8)+2;
			//r.nextInt() macht eine zufallszahl vom typ int
			//Math.abs() macht sie positiv
			//%x setzt die exclusive Obergrenze
			//+2 verhindert 0 und 1
		fraction = new FractionMaker();
		fraction.mkTwoFractions(10);
		numerator_1 = fraction.getNumerator_1() * factor;
		denominator_1 = fraction.getDenominator_1() * factor;
		
		tf_numerator_1.setText(String.valueOf(numerator_1));
		tf_denominator_1.setText(String.valueOf(denominator_1));
		pizza_1.drawPizzaAsCircle(numerator_1, denominator_1, Color.yellow);
		
		pizza_2.drawPizzaAsCircle(numerator_1, denominator_1, Color.yellow);
		
		lb_instruction.setText(lang.Messages.getString("reduce_max"));
		
		reducefactor = JFractionLab.greatestCommonDivisor(numerator_1, denominator_1);
		tf_numerator_2.setEditable(true);
		tf_numerator_2.requestFocusInWindow();
	}
	
	protected void nextProblem(){
		clearTextFields();
		tipIsSet = false;
		if (bl_randomProblem == true){
			makeProblem();
		}else{
			pizza_1.noPizzaAsCircle();
			pizza_2.noPizzaAsCircle();
			tf_numerator_1.setEditable(true);
			tf_numerator_1.requestFocusInWindow();
			
		}//if
	}
	
	/**
	 * 
	 */
	public void actionPerformed (ActionEvent e) {
		Object obj = e.getSource();
		if (obj == btn_continue){
			nextProblem();
		}else if (obj == btn_tipp){
			tipIsSet = true;
			new ReduceHintDialog(this, numerator_1,denominator_1);
		}else if (obj == btn_end){
			close_it();
		}else if (obj == rb_random){
			//System.out.println("rb_random");
			//the program is in "custom-mode" actually
			//it should switch to "random-mode"
			bl_randomProblem = true;
			nextProblem();
		}else if (obj == rb_custom){
			//System.out.println("rb_custom");
			//the program is in "random-mode" actually
			//it should switch to "custom-mode"
			bl_randomProblem = false;
			nextProblem();
		}else if(obj == jmiCreateWorkSheet){
			new WorkSheetDialog(
					ExerciseGenerator.REDUCE_FRACTIONS,
					lang.Messages.getString("reduce_fractions"),
					lang.Messages.getString("fractions"),
					lang.Messages.getString("factors")
			);
		}else if(obj == jmiHelp){
			new HelpStarter(
					lang.Messages.getLocale().toString(),
					"reduce");
		}
	}//actionPerformed
	/**
        wenn numerator passend gekrzt (optimale krzung wird nicht getesetet)
        	es geht weiter
        wenn denominator passend gekrzt und der numerator dazu passt
        	pizza pink
        	hinweis es geht besser
        	focus auf numeratoreingabe
	wenn denominator optimal gekrzt und der numerator dazu passt
		pizza gelb
		OK-Action
	*/
	public void keyPressed(KeyEvent event){
		Object obj = event.getSource();
		int key = event.getKeyCode();
		lb_info.setText("");
		if(obj == btn_continue & key == KeyEvent.VK_ENTER){
			nextProblem();
		}else if(obj == btn_end & key == KeyEvent.VK_ENTER){
			close_it();
			
		}else if(obj == tf_numerator_1 & key == KeyEvent.VK_ENTER){
			if(readInputInt(tf_numerator_1) != null){
				numerator_1 = readInputInt(tf_numerator_1);
				tf_numerator_1.setEditable(false);
				tf_denominator_1.setEditable(true);
				tf_denominator_1.requestFocusInWindow();
			}
		}else if(obj == tf_denominator_1 & key == KeyEvent.VK_ENTER){
			if(readInputInt(tf_denominator_1) != null){
				denominator_1 = readInputInt(tf_denominator_1);
				reducefactor = JFractionLab.greatestCommonDivisor(numerator_1, denominator_1);
				tf_denominator_1.setEditable(false);
				pizza_1.drawPizzaAsCircle(numerator_1, denominator_1, Color.yellow);
				pizza_2.drawPizzaAsCircle(numerator_1, denominator_1, Color.yellow);
				lb_instruction.setText(lang.Messages.getString("reduce_max"));
				tf_numerator_2.setEditable(true);
				tf_numerator_2.requestFocusInWindow();
			}
		}else if(obj == tf_numerator_2 & key == KeyEvent.VK_ENTER){
			if(readInputInt(tf_numerator_2) != null){
				int num2 = readInputInt(tf_numerator_2);
				System.out.println("numerator_1 / num2 = "+numerator_1 / num2);
				System.out.println("" +
						"denominator_1 % (numerator_1/num2) = "
						+denominator_1 % (numerator_1/num2)
				);
				if(
						numerator_1 % num2 != 0
						||
						denominator_1 % (numerator_1/num2)!=0
				){
					lb_info.setText(lang.Messages.getString("reduce_by_a_common_denominator"));
					tf_numerator_2.setText("");
				}else{
					tf_numerator_2.setEditable(false);
					tf_denominator_2.setEditable(true);
					tf_denominator_2.requestFocusInWindow();
				}
			}
		}else if(obj == tf_denominator_2 & key == KeyEvent.VK_ENTER){
			if(readInputInt(tf_denominator_2) != null){
				int valueOfZaehler2 = Integer.parseInt(tf_numerator_2.getText());
				int valueOfNenner2 = Integer.parseInt(tf_denominator_2.getText());
				if(
					denominator_1 % valueOfNenner2 == 0			//passend gekuerzt
					&&						//und
					denominator_1 / reducefactor != valueOfNenner2	//nicht optimal gekuerzt
				){
					if(denominator_1 / valueOfNenner2 == numerator_1 / valueOfZaehler2){ //passt zum numerator
						pizza_2.drawPizzaAsCircle(Integer.parseInt(tf_numerator_2.getText()), valueOfNenner2, Color.pink);
						lb_info.setText(lang.Messages.getString("reduce_better"));
						tf_denominator_2.setEditable(false);
						tf_numerator_2.setEditable(true);
						tf_numerator_2.requestFocusInWindow();
					}else{ //passt nicht zum numerator
						lb_info.setText(lang.Messages.getString("div_n_and_dn_with_same_nb"));
						tf_denominator_2.setText("");
						tf_denominator_2.setEditable(false);
						tf_numerator_2.setEditable(true);
						tf_numerator_2.requestFocusInWindow();
					}
				}else if (denominator_1 / reducefactor == valueOfNenner2){ //optimal gekrzt
					if(denominator_1 / valueOfNenner2 == numerator_1 / valueOfZaehler2){ //passt zum numerator
						if(!tipIsSet){
							points++;
							pdsp.setText(String.valueOf(points));
							owner.setPoints(points, "reduceFraction");
						}
						lb_info.setText(lang.Messages.getString("that_is_right")+" "+lang.Messages.getString("press_enter"));
						pizza_2.drawPizzaAsCircle(Integer.parseInt(tf_numerator_2.getText()), valueOfNenner2, Color.yellow);
						tf_denominator_2.setEditable(false);
						btn_continue.requestFocusInWindow();
					}else{
						lb_info.setText(lang.Messages.getString("div_n_and_dn_with_same_nb"));
						tf_denominator_2.setText("");
						tf_denominator_2.setEditable(false);
						tf_numerator_2.setEditable(true);
						tf_numerator_2.requestFocusInWindow();
					}
				}else{
					lb_info.setText(String.valueOf(denominator_1) +" / "+ String.valueOf(reducefactor) +" = ?");
					tf_denominator_2.setText("");
				}	
			}
		}
	}//keyPressed
	public void keyTyped(KeyEvent event){}
	public void keyReleased(KeyEvent event){}
	
}//class
