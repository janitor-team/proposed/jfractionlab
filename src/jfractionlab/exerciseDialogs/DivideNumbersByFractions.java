/**
 *	JFractionLab
 *	Copyright (C) 2005 jochen georges, gnugeo _ at _ gnugeo _ dot _ de
 *
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/
package jfractionlab.exerciseDialogs;

import info.clearthought.layout.TableLayout;

import java.awt.Color;
import java.awt.Container;
import java.awt.HeadlessException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JButton;
import javax.swing.JLabel;

import jfractionlab.FractionMaker;
import jfractionlab.HelpStarter;
import jfractionlab.JFractionLab;
import jfractionlab.MyJTextField;
import jfractionlab.displays.FractionAsRectangle;
import jfractionlab.displays.FractionLine;
import jfractionlab.exerciseGenerator.ExerciseGenerator;
import jfractionlab.jflDialogs.UsabilityDialog;
import jfractionlab.jflDialogs.WorkSheetDialog;





public class DivideNumbersByFractions extends ExerciseDialog implements ActionListener, KeyListener{
	static final long serialVersionUID = JFractionLab.serialVersionUID;
	//GUI
	//-----------------------------
	private MyJTextField tf_divident = new MyJTextField(2);
	private MyJTextField tf_aufgabe_numerator = new MyJTextField(2);
	private MyJTextField tf_aufgabe_denominator = new MyJTextField(2);
	
	private MyJTextField tf_kuerz_numerator1 = new MyJTextField(3);
	private MyJTextField tf_calculation_numerator1 =  new MyJTextField(3);
	private MyJTextField tf_kuerz_numerator2 = new MyJTextField(3);
	private MyJTextField tf_calculation_numerator2 = new MyJTextField(3);
	
	private MyJTextField tf_calculation_denominator = new MyJTextField(3);
	private MyJTextField tf_kuerz_denominator = new MyJTextField(3);
		
	private MyJTextField tf_zwergebnis_numerator = new MyJTextField(3);
	private MyJTextField tf_zwergebnis_denominator = new MyJTextField(3);
				
	private MyJTextField tf_endresult_ganzzahl = new MyJTextField(3);
	private MyJTextField tf_endresult_numerator = new MyJTextField(3);
	private MyJTextField tf_endresult_denominator = new MyJTextField(3);
	
	private MyJTextField tf_reduce_outer_numerator = new MyJTextField(3);
	private MyJTextField tf_reduce_inner_numerator = new MyJTextField(3);
	JLabel lb_frage = new JLabel("", JLabel.CENTER);
	FractionAsRectangle[] pizza = new FractionAsRectangle[8];
	FractionAsRectangle divisorpizza = new FractionAsRectangle();

	private int divident, numerator, denominator, zwischen_numerator, zwischen_denominator, ergebnis_ganzzahl, ergebnis_numerator;
	private boolean ergebnis_ist_glatt;

	/**
	 * 
	 * @param owner
	 * @param lx
	 * @param ly
	 * @param sx
	 * @param sy
	 * @throws HeadlessException
	 */
	public DivideNumbersByFractions(JFractionLab owner, int lx, int ly, int sx, int sy) throws HeadlessException {
		super(lx, ly, sx, sy);
		setTitle(lang.Messages.getString("div_the_nb"));
		btn_continue = new JButton(lang.Messages.getString("continue"));
		btn_end =  new JButton(lang.Messages.getString("end"));
		this.owner = owner;
		
		//Menu
		rb_random.addActionListener(this);
		jmOptions.add(rb_random);
		rb_custom.addActionListener(this);
		jmOptions.add(rb_custom);

		jmb.add(jmOptions);
		
		jmiCreateWorkSheet.addActionListener(this);
		jmWorkSheet.add(jmiCreateWorkSheet);
		jmb.add(jmWorkSheet);

		
		jmiHelp.addActionListener(this);
		jmb.add(jmHelp);
		setJMenuBar(jmb);
		
		double schmal = 0.03;
		double normal = 0.06;
		double breit = 0.09;
        	double sizes[][] = {{
			// Spalten
			breit,schmal,
			breit,schmal,
			schmal,normal,schmal, 
			normal,schmal,schmal, 
			breit,
			schmal, breit,
			normal, schmal,
			TableLayout.FILL
		},{
			//Zeilen
			TableLayout.FILL,
			TableLayout.FILL,
			12,
			TableLayout.FILL,
			TableLayout.FILL,
			TableLayout.FILL,
			0.3,
			0.3,
			30
		}};

		Container content = getContentPane();
        	content.setLayout(new TableLayout(sizes));
		content.setBackground(Color.white);
			addTextField(tf_divident);
			tf_divident.setEditable(false);
			tf_divident.addKeyListener(this);
		content.add(tf_divident, "0,1,0,3,r,c");
			JLabel lb_geteilt1 = new JLabel(":", JLabel.CENTER);
		content.add(lb_geteilt1, "1,2,c,c");
			addTextField(tf_aufgabe_numerator);
			tf_aufgabe_numerator.setEditable(false);
			tf_aufgabe_numerator.addKeyListener(this);
		content.add(tf_aufgabe_numerator, "2,1,c,b");
			FractionLine bs1 = new FractionLine();
		content.add(bs1, "2,2");
			addTextField(tf_aufgabe_denominator);
			tf_aufgabe_denominator.setEditable(false);
			tf_aufgabe_denominator.addKeyListener(this);
		content.add(tf_aufgabe_denominator, "2,3,c,t");
			JLabel lb_gleich1 =  new JLabel("=", JLabel.CENTER);
		content.add(lb_gleich1, "3,2, c, c");
			addTextField(tf_kuerz_numerator1);
			tf_kuerz_numerator1.addKeyListener(this);
			tf_kuerz_numerator1.setEditable(false);
		content.add(tf_kuerz_numerator1, "5,0,c,c");
			addTextField(tf_calculation_numerator1);
			tf_calculation_numerator1.setEditable(false);
		content.add(tf_calculation_numerator1, "5,1, c, c");
		JLabel lb_mal = new JLabel("*", JLabel.CENTER);
		content.add(lb_mal, "6,1,c,c");
			addTextField(tf_kuerz_numerator2);
			tf_kuerz_numerator2.setEditable(false);
			tf_kuerz_numerator2.addKeyListener(this);
		content.add(tf_kuerz_numerator2, "7,0, c, c");
			addTextField(tf_calculation_numerator2);
			tf_calculation_numerator2.setEditable(false);
			tf_calculation_numerator2.addKeyListener(this);
		content.add(tf_calculation_numerator2, "7,1, c, c");
		FractionLine bs2 = new FractionLine();
		content.add(bs2, "5,2, 7,2");
			addTextField(tf_calculation_denominator);
			tf_calculation_denominator.addKeyListener(this);
			tf_calculation_denominator.setEditable(false);
		content.add(tf_calculation_denominator, "7,3, c, c");
			addTextField(tf_kuerz_denominator);
			tf_kuerz_denominator.addKeyListener(this);
			tf_kuerz_denominator.setEditable(false);
		content.add(tf_kuerz_denominator, "7,4, c, c");
		JLabel lb_gleich2 =  new JLabel("=", JLabel.CENTER);
		content.add(lb_gleich2, "9,2, c, c");
			addTextField(tf_zwergebnis_numerator);
			tf_zwergebnis_numerator.setEditable(false);
			tf_zwergebnis_numerator.addKeyListener(this);
		content.add(tf_zwergebnis_numerator, "10,1, c, c");
		FractionLine bs3 = new FractionLine();
		content.add(bs3, "10,2");
			addTextField(tf_zwergebnis_denominator);
			tf_zwergebnis_denominator.setEditable(false);
			tf_zwergebnis_denominator.addKeyListener(this);
		content.add(tf_zwergebnis_denominator, "10,3, c, c");
		JLabel lb_gleich3 =  new JLabel("=", JLabel.CENTER);
		content.add(lb_gleich3, "11,2, c, c");
			addTextField(tf_endresult_ganzzahl);
			tf_endresult_ganzzahl.setEditable(false);
			tf_endresult_ganzzahl.addKeyListener(this);
		content.add(tf_endresult_ganzzahl, "12,1,12,3, c, c");
			addTextField(tf_endresult_numerator);
			tf_endresult_numerator.addKeyListener(this);
			tf_endresult_numerator.setEditable(false);
		content.add(tf_endresult_numerator, "13,1, c, c");
		FractionLine bs4 = new FractionLine();
		content.add(bs4, "13,2");
			addTextField(tf_endresult_denominator);
			tf_endresult_denominator.addKeyListener(this);
			tf_endresult_denominator.setEditable(false);
		content.add(tf_endresult_denominator, "13,3, c, c");
		content.add(lb_frage, "0,5,15,5,c,b");
		for (int i=0; i<8; i++){
			pizza[i] = new FractionAsRectangle();
		}
		content.add(pizza[0], "0,6,1,6");
		content.add(pizza[1], "2,6,3,6");
		content.add(pizza[2], "4,6,6,6");
		content.add(pizza[3], "7,6,9,6");
		content.add(pizza[4], "0,7,1,7");
		content.add(pizza[5], "2,7,3,7");
		content.add(pizza[6], "4,7,6,7");
		content.add(pizza[7], "7,7,9,7");
		JLabel lb_geteilt2 = new JLabel(":", JLabel.CENTER);
		content.add(lb_geteilt2, "10,6,c,c");
		content.add(divisorpizza, "11,6,12,6");
		btn_continue.addActionListener(this);
		btn_continue.addKeyListener(this);
		btn_continue.setFocusTraversalKeysEnabled(false);
		content.add(btn_continue, "15,1,c,c");
		btn_end.addActionListener(this);
		btn_end.addKeyListener(this);
		content.add(btn_end, "15,3,c,c");
		points = owner.points_divideNumbersByFractions;
		pdsp.setText(String.valueOf(points));
		content.add(pdsp, "15,6");
		lb_info.setFont(JFractionLab.infofont);
		lb_info.setText(lang.Messages.getString("you_just_need_nb_and_enter"));
		content.add(lb_info, "0,8,15,8");
		
		makeProblem();
		String[] ar_howto = {
			"howto_nb_and_enter","howto_option_type_of_exercise"
		};
		new UsabilityDialog(ar_howto);//new UsabilityDialog
	}//Konstruktor
	
	/**
	 * 
	 *
	 */
	protected void nextProblem(){
		for (int j=0; j<divident; j++){
			pizza[j].noPizzaAsRectangle();
		}
		clearTextFields();
		if (bl_randomProblem == true){
			makeProblem();
		}else{
			tf_divident.setEditable(true);
			tf_divident.requestFocusInWindow();
			divisorpizza.noPizzaAsRectangle();
		}//if - else
	}//public void nextProblem()

	/**
	 * 
	 *
	 */
	protected void makeProblem(){
		FractionMaker fraction1 = new FractionMaker();
		fraction1.mkOneFraction(9);
		numerator = fraction1.getNumerator_1();
		denominator = fraction1.getDenominator_1();
		try {
			Thread.sleep(1);
		}catch (InterruptedException e) {e.printStackTrace();}
		divident = (Math.abs(JFractionLab.ran.nextInt())%8)+1;
		//System.out.println("makeProblem(): "+numerator+"/"+denominator+", "+divident);
		//numerator = 2;
		//denominator = 3;
		//divident = 4;
		//Aufgabe
		tf_divident.setText(String.valueOf(divident));
		tf_aufgabe_numerator.setText(String.valueOf(numerator));
		tf_aufgabe_denominator.setText(String.valueOf(denominator));
		//Rechnung
		tf_calculation_numerator1.setText(String.valueOf(divident));
		tf_calculation_numerator1.setEditable(false);
		//Frage und Kuchen
		
		lb_frage.setText(lang.Messages.getString("how_often_fits_the_frac_in_the_nb"));
		drawDividentPizzen();
		divisorpizza.drawPizzaAsRectangle(numerator, denominator, Color.yellow, false,true);
		
		//Focus
		tf_calculation_numerator2.setEditable(true);
		tf_calculation_numerator2.requestFocusInWindow();
	}

	/**
	 * 
	 *
	 */
	private void drawDividentPizzen(){
		for (int j=0; j<divident; j++){
			pizza[j].drawPizzaAsRectangle(denominator, denominator, new Color(220, 220, 220), false,true);
		}//for
	}//drawDividentPizzen
	
	/**
	 * 
	 *
	 */
 	private void checkReducability(){
		if(JFractionLab.greatestCommonDivisor(Integer.parseInt(tf_calculation_numerator1.getText()),Integer.parseInt(tf_calculation_denominator.getText())) != 1){
			initReductionProcess(1);
		}else if(JFractionLab.greatestCommonDivisor(Integer.parseInt(tf_calculation_numerator2.getText()),Integer.parseInt(tf_calculation_denominator.getText())) != 1){
			initReductionProcess(2);
		}else{
			tf_zwergebnis_numerator.setEditable(true);
			tf_zwergebnis_numerator.requestFocusInWindow();
		}
	
	}//public void checkReducability()

 	/**
 	 * 
 	 * @param z
 	 */
	private void initReductionProcess(int z){
		//System.out.println("initReductionProcess("+z+")");
		if (z == 1){
			tf_reduce_outer_numerator = tf_kuerz_numerator1;
			tf_reduce_inner_numerator = tf_calculation_numerator1;
		}else if(z == 2){
			tf_reduce_outer_numerator = tf_kuerz_numerator2;
			tf_reduce_inner_numerator = tf_calculation_numerator2;
		}
		tf_kuerz_denominator.setEditable(true);
		tf_reduce_outer_numerator.setEditable(true);
		tf_reduce_outer_numerator.requestFocusInWindow();
	}//initReductionProcess

	/**
	 * 
	 */
	public void actionPerformed (ActionEvent e) {
		Object obj = e.getSource();
		if (obj == btn_continue){
			nextProblem();
		}else if (obj == btn_end){
			close_it();
		}else if (obj == rb_random){
			//System.out.println("rb_random");
			//the program is in "custom-mode" actually
			//it should switch to "random-mode"
			bl_randomProblem = true;
			nextProblem();
		}else if (obj == rb_custom){
			//System.out.println("rb_custom");
			//the program is in "random-mode" actually
			//it should switch to "custom-mode"
			bl_randomProblem = false;
			nextProblem();
		}else if(obj == jmiCreateWorkSheet){
			new WorkSheetDialog(
					ExerciseGenerator.DIVIDE_FRACTIONS,
					lang.Messages.getString("div_fr_by_fr"),
					lang.Messages.getString("fractions")
			);
		}else if(obj == jmiHelp){
			new HelpStarter(
					lang.Messages.getLocale().toString(),
					"divide-by-fractions"
			);
		}
	}//actionPerformed

	/**
	 * 
	 */
	public void keyPressed(KeyEvent event){
		Object obj = event.getSource();
		int key = event.getKeyCode();
		lb_info.setText("");
		if(obj == tf_divident & key == KeyEvent.VK_ENTER){
			if(readInputInt(tf_divident)!=null){
				divident = Integer.parseInt(tf_divident.getText());
				if (divident > 8){
					tf_divident.setText("");
					lb_info.setText(lang.Messages.getString("nb_max_eight"));
				}else{
					tf_calculation_numerator1.setText(String.valueOf(divident));
					tf_divident.setEditable(false);
					tf_aufgabe_numerator.setEditable(true);
					tf_aufgabe_numerator.requestFocusInWindow();
				}
			}
		}if(obj == tf_aufgabe_numerator & key == KeyEvent.VK_ENTER){
			if(readInputInt(tf_aufgabe_numerator)!=null){
				numerator = Integer.parseInt(tf_aufgabe_numerator.getText());
				tf_aufgabe_numerator.setEditable(false);
				tf_aufgabe_denominator.setEditable(true);
				tf_aufgabe_denominator.requestFocusInWindow();
			}
		}if(obj == tf_aufgabe_denominator & key == KeyEvent.VK_ENTER){
			if(readInputInt(tf_aufgabe_denominator)!=null){
				int pn = Integer.parseInt(tf_aufgabe_denominator.getText());
				if (pn > numerator){
					denominator = pn;
					tf_aufgabe_denominator.setEditable(false);
					drawDividentPizzen();
					divisorpizza.drawPizzaAsRectangle(numerator, denominator, Color.yellow, false,true);
					tf_calculation_numerator2.setEditable(true);
					tf_calculation_numerator2.requestFocusInWindow();
				}else{
					lb_info.setText(lang.Messages.getString("dn_must_be_bigger"));
					tf_aufgabe_denominator.setText("");
					tf_aufgabe_denominator.setEditable(false);
					tf_aufgabe_numerator.setText("");
					tf_aufgabe_numerator.setEditable(true);
					tf_aufgabe_numerator.requestFocusInWindow();
				}//if-else
			}
		}else if(obj == tf_calculation_numerator2 & key == KeyEvent.VK_ENTER){
			if(readInputInt(tf_calculation_numerator2)!=null){
				int z = Integer.parseInt(tf_calculation_numerator2.getText());
				if(z == denominator){
					tf_calculation_numerator2.setEditable(false);
					tf_calculation_denominator.setEditable(true);
					tf_calculation_denominator.requestFocusInWindow();
				}else{
					lb_info.setText(lang.Messages.getString("mk_reciprocal"));
					tf_calculation_numerator2.setText("");
				}
			}
		}else if(obj == tf_calculation_denominator & key == KeyEvent.VK_ENTER){
			if(readInputInt(tf_calculation_denominator)!=null){
				int z = Integer.parseInt(tf_calculation_denominator.getText());
				if(z == numerator){
					tf_calculation_denominator.setEditable(false);
					checkReducability();
				}else{
					//System.out.println("focusLost: Nenner ist nicht OK : ");
					lb_info.setText(lang.Messages.getString("mk_reciprocal"));
					tf_calculation_denominator.setText("");
				}
			}
		}else if(obj == tf_kuerz_numerator1 & key == KeyEvent.VK_ENTER){
			//System.out.println("tf_kuerz_numerator1 & key == KeyEvent.VK_ENTER");
			if(readInputInt(tf_kuerz_numerator1)!=null){
				tf_kuerz_numerator1.setEditable(false);
				tf_kuerz_denominator.setEditable(true);
				tf_kuerz_denominator.requestFocusInWindow();
			}
		}else if(obj == tf_kuerz_numerator2 & key == KeyEvent.VK_ENTER){
			if(readInputInt(tf_kuerz_numerator2)!=null){
				tf_kuerz_numerator2.setEditable(false);
				tf_kuerz_denominator.setEditable(true);
				tf_kuerz_denominator.requestFocusInWindow();
			}
		}else if(obj == tf_kuerz_denominator & key == KeyEvent.VK_ENTER){
			if(readInputInt(tf_kuerz_denominator)!=null){
				int z = Integer.parseInt(tf_reduce_inner_numerator.getText());
				int z_neu = Integer.parseInt(tf_reduce_outer_numerator.getText());
				int n = Integer.parseInt(tf_calculation_denominator.getText());
				int n_neu = Integer.parseInt(tf_kuerz_denominator.getText());
				//sind z o n vielfache oder teiler??
				if(inRow(z,z_neu) && inRow(n,n_neu)){
					if(JFractionLab.greatestCommonDivisor(z_neu,n_neu) != 1 || z/z_neu != n/n_neu){ //Es kann weitergekrzt werden
						tf_reduce_outer_numerator.setEditable(true);
						tf_reduce_outer_numerator.requestFocusInWindow();
						lb_info.setText(lang.Messages.getString("reduce_better"));
						tf_reduce_outer_numerator.setText("");
						tf_kuerz_denominator.setText("");	
					}else{//Es kann nicht weitergekrzt werden
						tf_reduce_outer_numerator.setEditable(false);
						tf_reduce_outer_numerator.setText("");
						tf_kuerz_denominator.setEditable(false);
						tf_reduce_inner_numerator.setText(String.valueOf(z_neu));
						tf_kuerz_denominator.setText("");
						tf_calculation_denominator.setText(String.valueOf(n_neu));
						checkReducability();
					}//if (JFractionLab.ggt(z,n) != 1)
				}else{//sind nicht in Reihe
					tf_reduce_outer_numerator.setEditable(true);
					lb_info.setText(lang.Messages.getString("div_n_and_dn_by_same_nb"));
					tf_reduce_outer_numerator.requestFocusInWindow();
					tf_reduce_outer_numerator.setText("");
					tf_kuerz_denominator.setText("");
				}//if in reihe
			}
		}else if(obj == tf_zwergebnis_numerator & key == KeyEvent.VK_ENTER){
			if(readInputInt(tf_zwergebnis_numerator)!=null){
				int z1 = Integer.parseInt(tf_calculation_numerator1.getText());
				int z2 = Integer.parseInt(tf_calculation_numerator2.getText());
				int zz = Integer.parseInt(tf_zwergebnis_numerator.getText());
				//System.out.println("focusLost obj == tf_zwergebnis_numerator -- if(readInputNumber()!=null){");
				//System.out.println(z1+", "+z2+", "+zz);
				if(z1 * z2 == zz){
					zwischen_numerator = zz;
					tf_zwergebnis_numerator.setEditable(false);
					tf_zwergebnis_denominator.setEditable(true);
					tf_zwergebnis_denominator.requestFocusInWindow();
				}else{
					//System.out.println("focusLost obj == tf_zwergebnis_numerator zahl ist falsch");
					lb_info.setText(lang.Messages.getString("multiply_nums"));
					tf_zwergebnis_numerator.setText("");
				}//else
			}
		}else if(obj == tf_zwergebnis_denominator & key == KeyEvent.VK_ENTER){
			if(readInputInt(tf_zwergebnis_denominator)!=null){
				int re_n = Integer.parseInt(tf_calculation_denominator.getText());
				zwischen_denominator = Integer.parseInt(tf_zwergebnis_denominator.getText());
				zwischen_numerator = Integer.parseInt(tf_zwergebnis_numerator.getText());
				if(zwischen_denominator == re_n){
					tf_zwergebnis_denominator.setEditable(false);
					if (zwischen_numerator > zwischen_denominator){
						//System.out.println("zwischen_numerator > zwischen_denominator = "+zwischen_numerator+" > "+zwischen_denominator);
						tf_endresult_ganzzahl.setEditable(true);
						tf_endresult_ganzzahl.requestFocusInWindow();
						//System.out.println("!!!!!zwischen_numerator%zwischen_denominator = "+zwischen_numerator%zwischen_denominator);
						if (zwischen_numerator%zwischen_denominator == 0){
							//System.out.println("ergebnis_ist_glatt = true");
							ergebnis_ist_glatt = true;
						}else{
							//System.out.println("ergebnis_ist_glatt = false");
							ergebnis_ist_glatt = false;
						}
					}else{
						//System.out.println("Ergebnis ist kleiner als 1 - Das ist unm�lich!!???");
						//tf_endresult_numerator.setEditable(true);
					}
				}else{
					lb_info.setText(lang.Messages.getString("num_stays_unchanged"));
					tf_zwergebnis_denominator.setText("");
				}//if(cd1 * cd2 == en)
			}
		}else if(obj == tf_endresult_ganzzahl & key == KeyEvent.VK_ENTER){
			if(readInputInt(tf_endresult_ganzzahl)!=null){
				ergebnis_ganzzahl= Integer.parseInt(tf_endresult_ganzzahl.getText());
				//System.out.println("zwischen_numerator/zwischen_denominator = "+zwischen_numerator/zwischen_denominator);
				if(ergebnis_ganzzahl == zwischen_numerator/zwischen_denominator){
					tf_endresult_ganzzahl.setEditable(false);
					if(ergebnis_ist_glatt){
						//System.out.println("if(ergebnis_ist_glatt) true :-)");
						if(bl_randomProblem){points++;};
						pdsp.setText(String.valueOf(points));
						owner.setPoints(points, "divideNumbersByFractions");
						lb_info.setText(lang.Messages.getString("that_is_right"));
						btn_continue.requestFocusInWindow();
					}else{
						tf_endresult_numerator.setEditable(true);
						tf_endresult_numerator.requestFocusInWindow();
					}
				}else{
					lb_info.setText(lang.Messages.getString("how_often_fits_the_denominator_in_the_numerator"));
					tf_endresult_ganzzahl.setText("");
				}
			}
		}else if(obj == tf_endresult_numerator & key == KeyEvent.VK_ENTER){
			if(readInputInt(tf_endresult_numerator)!=null){
				ergebnis_numerator = Integer.parseInt(tf_endresult_numerator.getText());
				int bla = zwischen_numerator - ergebnis_ganzzahl * zwischen_denominator;
				//System.out.println("zwischen_numerator - ergebnis_ganzzahl * zwischen_denominator = "+bla);
				if(ergebnis_numerator == bla){
					tf_endresult_numerator.setEditable(false);
					tf_endresult_denominator.setEditable(true);
					tf_endresult_denominator.requestFocusInWindow();
				}else{
					//lb_info.setText(lang.Messages.getString(""));
					lb_info.setText(
						String.valueOf(zwischen_numerator) +
						" - " +
						String.valueOf(ergebnis_ganzzahl) +
						" * " +
						String.valueOf(zwischen_denominator) +
						" = ?"
					);
					tf_endresult_numerator.setText("");
				}
			}
		}else if(obj == tf_endresult_denominator & key == KeyEvent.VK_ENTER){
			if(readInputInt(tf_endresult_denominator)!=null){
				int ergebnis_denominator = Integer.parseInt(tf_endresult_denominator.getText());
				if(ergebnis_denominator == zwischen_denominator){
					tf_endresult_denominator.setEditable(false);
					if(bl_randomProblem){points++;};
					pdsp.setText(String.valueOf(points));
					owner.setPoints(points, "divideNumbersByFractions");
					lb_info.setText(lang.Messages.getString("that_is_right"));
					btn_continue.requestFocusInWindow();
				}else{
					lb_info.setText(lang.Messages.getString("num_stays_unchanged"));
					tf_endresult_denominator.setText("");
				}
			}
		}else if(obj == btn_continue & key == KeyEvent.VK_ENTER){
			nextProblem();
		}else if(obj == btn_end & key == KeyEvent.VK_ENTER){
			close_it();
		}
	}//keyPressed
	public void keyTyped(KeyEvent event){}
	public void keyReleased(KeyEvent event){}
	

	/**
	 * 
	 * @param n
	 * @param nn
	 * @return
	 */
	private boolean inRow(int n, int nn){
		boolean wert = false;
		if (nn>n){
			if(nn%n == 0){
				wert = true;
			}else{
				wert = false;
			}
		}else if (nn<n){
			if(n%nn == 0){
				wert = true;
			}else{
				wert = false;
			}
		}else if (nn == n){ wert =  true;}
		return wert;
	}//inRow(int n, int nn)

}//class
