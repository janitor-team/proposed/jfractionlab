/**
 *	JFractionLab
 *	Copyright (C) 2005 jochen georges, gnugeo _ at _ gnugeo _ dot _ de
 *
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/

package jfractionlab.exerciseDialogs;

import info.clearthought.layout.TableLayout;

import java.awt.Color;
import java.awt.Container;
import java.awt.HeadlessException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JButton;
import javax.swing.JLabel;

import jfractionlab.FractionMaker;
import jfractionlab.HelpStarter;
import jfractionlab.JFractionLab;
import jfractionlab.MyJTextField;
import jfractionlab.displays.FractionAsCircle;
import jfractionlab.displays.FractionLine;
import jfractionlab.exerciseGenerator.ExerciseGenerator;
import jfractionlab.jflDialogs.UsabilityDialog;
import jfractionlab.jflDialogs.WorkSheetDialog;




public class ExtendFraction extends ExerciseDialog implements ActionListener, KeyListener{
	static final long serialVersionUID = JFractionLab.serialVersionUID;
	//GUI
	private JLabel lb_instruction = new JLabel("", JLabel.CENTER);
	private JLabel lb_numerator_1 = new JLabel("", JLabel.CENTER);
	private JLabel lb_denominator_1 = new JLabel("", JLabel.CENTER);
	private MyJTextField tf_numerator_2 = new MyJTextField(2);
	private MyJTextField tf_denominator_2 = new MyJTextField(2);
	private FractionAsCircle pp_pizza_1 = new FractionAsCircle();
	private FractionAsCircle pp_pizza_2 = new FractionAsCircle();
	private JLabel lb_equal = new JLabel("", JLabel.CENTER);
	//Zahlen
	private FractionMaker fraction;
	private int numerator_1;
	private int denominator_1;
	private int numerator_2;
	private int denominator_2;
	private int extendfactor;
	

	
	/**
	 * 
	 * @param owner
	 * @param lx
	 * @param ly
	 * @param sx
	 * @param sy
	 * @throws HeadlessException
	 */
	public ExtendFraction(JFractionLab owner, int lx, int ly, int sx, int sy) throws HeadlessException {
		super(lx,ly,sx,sy);
		setTitle(lang.Messages.getString("extend_the_fraction"));
		btn_continue = new JButton(lang.Messages.getString("continue"));
		btn_end =  new JButton(lang.Messages.getString("end"));
		this.owner = owner;
		//Menu		
		jmiCreateWorkSheet.addActionListener(this);
		jmWorkSheet.add(jmiCreateWorkSheet);
		jmb.add(jmWorkSheet);
//		else if(obj == jmiCreateWorkSheet){
//			new WorkSheetDialogMultiplication();
//		}
		jmiHelp.addActionListener(this);
		jmb.add(jmHelp);
		setJMenuBar(jmb);
//		
		double sizes[][] = {{	
			//Spalten
			5,
			TableLayout.FILL,0.06,TableLayout.FILL,
			10,10,
			TableLayout.FILL,0.06,TableLayout.FILL,
			0.15,
			5
		}, {
			//Zeilen
			5,
			30,
			TableLayout.FILL,
			12,
			TableLayout.FILL,
			0.8,
			30,
			5
		}};
		Container content = getContentPane();
        	content.setLayout(new TableLayout(sizes));
		content.setBackground(Color.white);
		
			lb_instruction.setFont(JFractionLab.infofont);
		content.add(lb_instruction, "0,1,8,0");
		content.add(lb_numerator_1, "2,2,c,b");
		FractionLine bs_1 = new FractionLine();
		content.add(bs_1, "2,3");
		content.add(lb_denominator_1, "2,4,c,t");
		content.add(pp_pizza_1, "1,5,4,5");
			lb_equal.setFont(JFractionLab.infofont);
			lb_equal.setText("=");
		content.add(lb_equal, "4,3,5,3,f,c");
			addTextField(tf_numerator_2);
			tf_numerator_2.addKeyListener(this);
		content.add(tf_numerator_2, "7,2,c,b");
		FractionLine bs_2 = new FractionLine();
		content.add(bs_2, "7,3");
			addTextField(tf_denominator_2);
			tf_denominator_2.addKeyListener(this);
		content.add(tf_denominator_2, "7,4,c,t");
		content.add(pp_pizza_2, "5,5,8,5");
			btn_continue.addKeyListener(this);
			btn_continue.addActionListener(this);
		content.add(btn_continue, "9,2,f,b");
			//der Button zeigt komisches Verhalten
			//Das Hauptfenster bleibt fuer Sekunden grau, bis sich dessen Oberflaeche neu aufbaut
			//warum ist das hier und nicht bei den anderen Uebungen?
			btn_end.addActionListener(this);
		content.add(btn_end, "9,4,f,t");
			points = owner.points_extendFraction;
			//System.out.println("points = " + String.valueOf(points));
			//pdsp.setBackground(Color.red);
			pdsp.setText(String.valueOf(points));
		content.add(pdsp, "9,5");
		//content.add(pdsp, "9,5,f,b"); //Warum geht das nicht??????
			lb_info.setFont(JFractionLab.infofont);
			lb_info.setText(lang.Messages.getString("you_just_need_nb_and_enter"));
		content.add(lb_info, "0,6,8,6");
		makeProblem();
		String[] ar_howto = {"howto_nb_and_enter"};
		new UsabilityDialog(ar_howto);//new UsabilityDialog
	}//Konstruktor
	
	/**
	 * 
	 *
	 */
	protected void makeProblem(){
		//System.out.println("makeProblem");
		fraction = new FractionMaker();
		fraction.mkTwoFractions(10);
		numerator_1 = fraction.getNumerator_1();
		denominator_1 = fraction.getDenominator_1();
		
		lb_numerator_1.setText(String.valueOf(numerator_1));
		lb_denominator_1.setText(String.valueOf(denominator_1));
		pp_pizza_1.drawPizzaAsCircle(numerator_1, denominator_1, Color.yellow);
		
		pp_pizza_2.drawPizzaAsCircle(numerator_1, denominator_1, Color.yellow);
		
		extendfactor = (Math.abs(JFractionLab.ran.nextInt())%10)+2;
			//r.nextInt() macht eine zufallszahl vom typ int
			//Math.abs() macht sie positiv
			//%x setzt die exclusive Obergrenze
			//+2 verhindert 0 und 1
		lb_instruction.setText(lang.Messages.getString("extend_with") + " " + String.valueOf(extendfactor));
		tf_numerator_2.setEditable(true);
		tf_numerator_2.requestFocusInWindow();
	}
	
	/**
	 * 
	 *
	 */
	protected void nextProblem(){
		clearTextFields();
		lb_info.setText("");
		makeProblem();
	}
	
	/**
	 * 
	 */
	public void actionPerformed (ActionEvent e) {
		Object obj = e.getSource();
       	if (obj == btn_continue){
			nextProblem();
		}else if (obj == btn_end){
			close_it();
		}else if(obj == jmiCreateWorkSheet){
			new WorkSheetDialog(
					ExerciseGenerator.EXTEND_FRACTIONS,
					lang.Messages.getString("extend_fractions"),
					lang.Messages.getString("fractions"),
					lang.Messages.getString("factors")
			);
		}else if(obj == jmiHelp){
			new HelpStarter(
					lang.Messages.getLocale().toString(),
					"extend"
			);
		}
	}//actionPerformed
    public void keyPressed(KeyEvent event){
    	Object obj = event.getSource();
		int key = event.getKeyCode();
		lb_info.setText("");
		if(obj == btn_continue & key == KeyEvent.VK_ENTER){
			nextProblem();
		}else if(obj == btn_end & key == KeyEvent.VK_ENTER){
			close_it();
		}else if(obj == tf_numerator_2 & key == KeyEvent.VK_ENTER){
			if(readInputInt(tf_numerator_2) != null){
				if (numerator_1 * extendfactor != readInputInt(tf_numerator_2)){
					lb_info.setText(String.valueOf(numerator_1) +" * "+ String.valueOf(extendfactor) +" = ?");
					tf_numerator_2.setText("");
				}else{
					tf_numerator_2.setEditable(false);
					tf_denominator_2.setEditable(true);
					tf_denominator_2.requestFocusInWindow();
					numerator_2 = numerator_1 * extendfactor;
				}
			}
		}else if(obj == tf_denominator_2 & key == KeyEvent.VK_ENTER){
			if(readInputInt(tf_denominator_2) != null){
				if (denominator_1 * extendfactor != readInputInt(tf_denominator_2)){
					lb_info.setText(String.valueOf(denominator_1) +" * "+ String.valueOf(extendfactor) +" = ?");
					tf_denominator_2.setText("");
				}else{
					denominator_2 = denominator_1 * extendfactor;
					points++;
					pdsp.setText(String.valueOf(points));
					owner.setPoints(points, "extendFraction");
					lb_info.setText(lang.Messages.getString("that_is_right")+" "+lang.Messages.getString("press_enter"));
					pp_pizza_2.drawPizzaAsCircle(numerator_2, denominator_2, Color.yellow);
					tf_denominator_2.setEditable(false);
					btn_continue.requestFocusInWindow();
				}	
			}
		}
	}//keyPressed
	public void keyTyped(KeyEvent event){}
	public void keyReleased(KeyEvent event){}
}//class
