/**
 *	JFractionLab
 *	Copyright (C) 2005 jochen georges, gnugeo _ at _ gnugeo _ dot _ de
 *
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/

package jfractionlab.exerciseDialogs;

import info.clearthought.layout.TableLayout;

import java.awt.Color;
import java.awt.Container;
import java.awt.HeadlessException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;

import jfractionlab.FractionMaker;
import jfractionlab.HelpStarter;
import jfractionlab.JFractionLab;
import jfractionlab.displays.FractionAsClickCircle;
import jfractionlab.displays.FractionLine;
import jfractionlab.jflDialogs.UsabilityDialog;

/**
 * @author jochen
 *
 */

public class ClickNumerator extends ExerciseDialog implements ActionListener{
	static final long serialVersionUID = JFractionLab.serialVersionUID;
	//GUI
	private JLabel lb_numerator = new JLabel("", JLabel.CENTER);
	private JLabel lb_denominator = new JLabel("", JLabel.CENTER);
	private FractionAsClickCircle clickablePizza = new FractionAsClickCircle(this);

	//Zahlen
	private FractionMaker fraction;
	private int numerator;
	private int denominator;
	
	
	/**
	 * 
	 * @param owner
	 * @param lx
	 * @param ly
	 * @param sx
	 * @param sy
	 * @throws HeadlessException
	 */
	public ClickNumerator(JFractionLab owner, int lx, int ly, int sx, int sy) throws HeadlessException {
		super(lx, ly, sx, sy);
		setTitle(lang.Messages.getString("click_the_numerator"));
		btn_continue = new JButton(lang.Messages.getString("continue"));
		btn_end =  new JButton(lang.Messages.getString("end"));
		this.owner = owner;
		
		jmiHelp.addActionListener(this);
		jmb.add(jmHelp);
		setJMenuBar(jmb);
		
		double sizes[][] = {{	
			// Spalten
			TableLayout.FILL,
			0.10,
			TableLayout.FILL,
			0.20,
			12
		},{	
			//Zeilen
			30,
			12,
			30,
			TableLayout.FILL,
			TableLayout.FILL,
			12,
			TableLayout.FILL,
			TableLayout.FILL,
			30
		}};

		Container content = getContentPane();
        	content.setLayout(new TableLayout(sizes));
		content.setBackground(Color.white);
		content.add(lb_numerator, "1,0");
		FractionLine bs = new FractionLine();
		content.add(bs, "1,1");
		content.add(lb_denominator, "1,2");
		content.add(clickablePizza, "0,3,2,7");
		btn_continue.addActionListener(this);
		content.add(btn_continue, "3,4,f,b");
		btn_end.addActionListener(this);
		content.add(btn_end, "3,6,f,t");
		points = owner.points_clickNumerator;
		pdsp.setText(String.valueOf(points));
		content.add(pdsp, "3,7,3,8");
		lb_info.setFont(JFractionLab.infofont);
		lb_info.setText(lang.Messages.getString("use_the_mouse"));
		content.add(lb_info, "0,8,2,8");

		makeProblem();
		String[] ar_howto = {"howto_click_numerator"};
		new UsabilityDialog(ar_howto);//new UsabilityDialog
	}//Konstruktor
	
	protected void nextProblem(){
		if (clickablePizza.check() == "0"){
			points++;
			pdsp.setText(String.valueOf(points));
			owner.setPoints(points, "clickNumerator");
			makeProblem();
		}else if (clickablePizza.check() == "+"){
			lb_info.setText(lang.Messages.getString("delete_with_rightclick"));
		}else if (clickablePizza.check() == "-"){
			lb_info.setText(lang.Messages.getString("not_enough"));
		}
	}
	/**
	 * 
	 *
	 */
	protected void makeProblem(){
		//System.out.println("makeProblem");
		fraction = new FractionMaker();
		fraction.mkOneFraction(10); //der Parameter setzt die exclusive Obergrenze der Zahlen des Bruches
		numerator = fraction.getNumerator_1();
		denominator = fraction.getDenominator_1();
		lb_numerator.setText(String.valueOf(numerator));
		lb_denominator.setText(String.valueOf(denominator));
		lb_info.setText("");
		clickablePizza.setFraction(numerator, denominator);
	}//makeProblem
	
	/**
	 * 
	 */
	public void actionPerformed (ActionEvent e) {
		Object obj = e.getSource();
		if (obj == btn_continue){
			nextProblem();
		}else if (obj == btn_end){
			close_it();
		}else if(obj == jmiHelp){
			new HelpStarter(
					lang.Messages.getLocale().toString(),
					"fractions-explanation"
			);
		}
        }//actionPerformed
}//class
