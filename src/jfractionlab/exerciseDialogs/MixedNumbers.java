/**
 *	JFractionLab
 *	Copyright (C) 2005 jochen georges, gnugeo _ at _ gnugeo _ dot _ de
 *
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/

package jfractionlab.exerciseDialogs;

import info.clearthought.layout.TableLayout;

import java.awt.Color;
import java.awt.Container;
import java.awt.HeadlessException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JButton;
import javax.swing.JLabel;

import jfractionlab.FractionMaker;
import jfractionlab.HelpStarter;
import jfractionlab.JFractionLab;
import jfractionlab.MyJTextField;
import jfractionlab.displays.FractionAsCircle;
import jfractionlab.displays.FractionLine;
import jfractionlab.exerciseGenerator.ExerciseGenerator;
import jfractionlab.jflDialogs.UsabilityDialog;
import jfractionlab.jflDialogs.WorkSheetDialog;

public class MixedNumbers extends ExerciseDialog implements ActionListener, KeyListener{
	static final long serialVersionUID = JFractionLab.serialVersionUID;
		//GUI
		
		private MyJTextField tf_mixedNBNumber = new MyJTextField(2);
		private MyJTextField tf_mixedNBNumerator = new MyJTextField(2);
		private MyJTextField tf_mixedNBDenominator = new MyJTextField(2);
		
		private MyJTextField tf_improperFractionNumerator = new MyJTextField(2);
		private MyJTextField tf_improperFractionDenominator = new MyJTextField(2);

		private FractionAsCircle[] pizzaPlace = {
				new FractionAsCircle(),
				new FractionAsCircle(),
				new FractionAsCircle(),
				new FractionAsCircle(),
				new FractionAsCircle(),
				new FractionAsCircle(),
				new FractionAsCircle(),
				new FractionAsCircle(),
				new FractionAsCircle()
		};

		private JButton btn_tipp;

		private boolean isVisible = true;
		private boolean was_neverVisible = true;
		//Zahlen
		private FractionMaker fm;
		private int mixedNBNumber;
		private int mixedNBNumerator;
		private int improperFractionNumerator;
		private int improperFractionDenominator;
		/**
		 * 
		 * @param owner
		 * @param lx
		 * @param ly
		 * @param sx
		 * @param sy
		 * @throws HeadlessException
		 */
		public MixedNumbers(JFractionLab owner, int lx, int ly, int sx, int sy) throws HeadlessException {
			super(lx, ly, sx, sy);
			setTitle(lang.Messages.getString("mixed_numbers_2_improper_fracs"));
			this.owner = owner;
			
			//Menu
			rb_random.addActionListener(this);
			jmOptions.add(rb_random);
			rb_custom.addActionListener(this);
			jmOptions.add(rb_custom);
			jmOptions.addSeparator();
			
			rb_visible.addActionListener(this);
			jmOptions.add(rb_visible);
			rb_invisible.addActionListener(this);
			jmOptions.add(rb_invisible);
			jmb.add(jmOptions);
			jmiCreateWorkSheet.addActionListener(this);
			jmWorkSheet.add(jmiCreateWorkSheet);
			jmb.add(jmWorkSheet);
			jmiHelp.addActionListener(this);
			jmb.add(jmHelp);
			setJMenuBar(jmb);

			double width_of_pizzaplace = 0.45;
			double sizes[][] = {{
				// Spalten
				5,//0
				width_of_pizzaplace/4,//1
				width_of_pizzaplace/4,//2
				width_of_pizzaplace/4,//3
				width_of_pizzaplace/4,//4	
				12,//5
				width_of_pizzaplace/6,//6
				width_of_pizzaplace/6,//7
				width_of_pizzaplace/6,//8
				width_of_pizzaplace/6,//9g.setColor(getForeground());
				width_of_pizzaplace/6,//10
				width_of_pizzaplace/6,//11
				TableLayout.FILL,//12
				5//13
			},{
				//Zeilen
				5,//0jflTextResource.properties
				TableLayout.FILL,//1
				20,//2
				TableLayout.FILL,//3
				0.4,//4
				0.4,//5
				TableLayout.FILL,//6
				5//7
			}};
			Container content = getContentPane();
			content.setLayout(new TableLayout(sizes));
			content.setBackground(Color.white);
			//--links
				addTextField(tf_mixedNBNumber);
				tf_mixedNBNumber.addKeyListener(this);
				tf_mixedNBNumber.setEditable(false);
			content.add(tf_mixedNBNumber, "2,1,2,3,c,c");
				addTextField(tf_mixedNBNumerator);
				tf_mixedNBNumerator.addKeyListener(this);
				tf_mixedNBNumerator.setEditable(false);
			content.add(tf_mixedNBNumerator, "3,1,c,b");
				addTextField(tf_mixedNBDenominator);
				tf_mixedNBDenominator.addKeyListener(this);
				tf_mixedNBDenominator.setEditable(false);
			content.add(tf_mixedNBDenominator, "3,3,c,t");
			//--rechts
				addTextField(tf_improperFractionNumerator);
				tf_improperFractionNumerator.addKeyListener(this);
				tf_improperFractionNumerator.setEditable(false);
			content.add(tf_improperFractionNumerator, "8,1,9,1,c,b");
				addTextField(tf_improperFractionDenominator);
				tf_improperFractionDenominator.setEditable(false);
			content.add(tf_improperFractionDenominator, "8,3,9,3,c,t");

			//--Zeichen
			JLabel lb_gleich1 = new JLabel("=", JLabel.CENTER);
			content.add(lb_gleich1, "5,2");
			FractionLine bs_fraction1 = new FractionLine();
			content.add(bs_fraction1, "3,2");
			FractionLine bs_fraction2 = new FractionLine();
			content.add(bs_fraction2, "8,2,9,2");
			//--Pizzen
			content.add(pizzaPlace[0], "1,4,2,4");
			content.add(pizzaPlace[1], "1,5,2,5");
			content.add(pizzaPlace[2], "3,4,4,4");
			content.add(pizzaPlace[3], "3,5,5,5");
			content.add(pizzaPlace[4], "6,4,8,4");
			content.add(pizzaPlace[5], "6,5,8,5");
			content.add(pizzaPlace[6], "9,4,11,4");
			content.add(pizzaPlace[7], "9,5,11,5");
			//--Controls
				btn_continue = new JButton(lang.Messages.getString("continue"));
				btn_continue.addActionListener(this);
				btn_continue.addKeyListener(this);
				btn_continue.setFocusTraversalKeysEnabled(false);
			content.add(btn_continue, "12,1,f,c");
				btn_tipp = new JButton(lang.Messages.getString("Tipp"));
				btn_tipp.addActionListener(this);
				btn_tipp.setEnabled(false);
			content.add(btn_tipp, "12,4,f,c");
				btn_end =  new JButton(lang.Messages.getString("end"));
				btn_end.addActionListener(this);
			content.add(btn_end, "12,3,f,c");
			content.add(pdsp, "12,5");
				lb_info.setFont(JFractionLab.infofont);
				lb_info.setText(lang.Messages.getString("you_just_need_nb_and_enter"));
			content.add(lb_info, "1,6,11,6");

			points = owner.points_gemischteZahlen;
			pdsp.setText(String.valueOf(points));
			makeProblem();
			String[] ar_howto = {
				"howto_nb_and_enter","howto_option_type_of_exercise","howto_option_invisible"
			};
			new UsabilityDialog(ar_howto);//new UsabilityDialog
		}//Konstruktor
		
		/**
		 * 
		 *
		 */	
		protected void makeProblem(){
			//System.out.println("=== makeProblem ===");
			fm = new FractionMaker(); //der Parameter setzt die exclusive Obergrenze der Zahlen des Bruches
			fm.mkImproperFraction();
			improperFractionNumerator = fm.getNumerator_1();
			improperFractionDenominator = fm.getDenominator_1();
				//TESTZAHLEN
				//improperFractionNumerator = 16; improperFractionDenominator = 8;
			mixedNBNumber = improperFractionNumerator/improperFractionDenominator;
			mixedNBNumerator = improperFractionNumerator - improperFractionDenominator * mixedNBNumber;
			
			tf_improperFractionDenominator.setText(String.valueOf(improperFractionDenominator));
			was_neverVisible = true;
			if(isVisible){
				drawPizzaBlech();
				tf_mixedNBNumber.setEditable(true);
				tf_mixedNBNumber.requestFocusInWindow();
				lb_info.setText(lang.Messages.getString("which_mixed_number_do_you_see"));
			}else{
				tf_mixedNBNumber.setText(String.valueOf(mixedNBNumber));
				if(mixedNBNumerator != 0){tf_mixedNBNumerator.setText(String.valueOf(mixedNBNumerator));}
				if(mixedNBNumerator != 0){tf_mixedNBDenominator.setText(String.valueOf(improperFractionDenominator));}
				tf_improperFractionNumerator.setEditable(true);
				tf_improperFractionNumerator.requestFocusInWindow();
			}
		}//makeProblem()
		
		/**
		 * 
		 *
		 */
		private void drawPizzaBlech(){
			int nb = 0;
			for(int i = 0; i < mixedNBNumber; i++){
				pizzaPlace[i].drawPizzaAsCircle(improperFractionDenominator, improperFractionDenominator, Color.yellow);
				nb = i;
			}//for
			if(mixedNBNumerator != 0){
				pizzaPlace[nb+1].drawPizzaAsCircle(
						mixedNBNumerator,
						improperFractionDenominator,
						Color.yellow);
			}else{
				pizzaPlace[nb+1].noPizzaAsCircle();
			}//if	
			was_neverVisible = false;
		}//drawPizzaBlech
		
		/**
		 * 
		 *
		 */
		private void clearPizzaBlech(){
			for(int i = 0; i < 8; i++){
				pizzaPlace[i].noPizzaAsCircle();
			}//for
		}//clearPizzaBlech
		
		/**
		 * 
		 *
		 */	
		protected void nextProblem(){
			clearTextFields();
			clearPizzaBlech();
			if (bl_randomProblem == true){
				makeProblem();
			}else{
				tf_mixedNBNumber.setEditable(true);
				tf_mixedNBNumber.requestFocusInWindow();
			}//if
		}

		/**
		 * 
		 *
		 *
		private void close_it(){
			setVisible(false);
			dispose();
		}//close_it()
	*/
		/**
		 * 
		 */
		public void actionPerformed (ActionEvent e) {
			Object obj = e.getSource();
			if (obj == btn_continue){
				if (isVisible){
					btn_tipp.setEnabled(false);
				}else{
					btn_tipp.setEnabled(true);
				}
				nextProblem();
			}else if (obj == btn_end){
				close_it();
			}else if (obj == btn_tipp){
				//System.out.println("btn_tipp");
				drawPizzaBlech();
				btn_tipp.setEnabled(false);
				tf_improperFractionNumerator.requestFocusInWindow();
			}else if (obj == rb_visible){
				//System.out.println("rb_visble");
				//the program is in "Invisible-mode" actually
				//but it should switch to "Visible-mode"
				if(!bl_randomProblem){drawPizzaBlech();}
				btn_tipp.setEnabled(false);
				isVisible = true;
			}else if (obj == rb_invisible){
				//System.out.println("rb_invisble");
				//the program is in "Visible-mode" actually
				//it should switch to "Invisible-mode"
				clearPizzaBlech();
				btn_tipp.setEnabled(true);
				isVisible = false;
				makeProblem();
			}else if (obj == rb_random){
				//System.out.println("rb_random");
				//the program is in "custom-mode" actually
				//it should switch to "random-mode"
				tf_mixedNBNumber.setEditable(false);
				bl_randomProblem = true;
				nextProblem();
			}else if (obj == rb_custom){
				//System.out.println("rb_custom");
				//the program is in "random-mode" actually
				//it should switch to "custom-mode"
				bl_randomProblem = false;
				tf_improperFractionDenominator.setText("");
				nextProblem();
			}else if(obj == jmiCreateWorkSheet){
				new WorkSheetDialog(
						ExerciseGenerator.MIXED_NUMBERS,
						lang.Messages.getString("mixed_numbers"),
						lang.Messages.getString("full_numbers"),
						lang.Messages.getString("fractions")
				);
			}else if(obj == jmiHelp){
				new HelpStarter(
						lang.Messages.getLocale().toString(),
						"mixed-numbers"
				);
			}
		}//actionPerformed
		
		/**
		 * 
		 */
		public void keyPressed(KeyEvent event){
			Object obj = event.getSource();
			int key = event.getKeyCode();
			lb_info.setText("");
			if(obj == btn_continue & key == KeyEvent.VK_ENTER){
				nextProblem();
			}else if(obj == tf_mixedNBNumber & key == KeyEvent.VK_ENTER){
				if(readInputInt(tf_mixedNBNumber) != null){
					int num = readInputInt(tf_mixedNBNumber);
					if(bl_randomProblem){
						if(num == mixedNBNumber){
							tf_mixedNBNumber.setEditable(false);
							if(mixedNBNumerator != 0){
								tf_mixedNBNumerator.setEditable(true);
								tf_mixedNBNumerator.requestFocusInWindow();
							}else{
								tf_improperFractionNumerator.setEditable(true);
								tf_improperFractionNumerator.requestFocusInWindow();
							}
						}else{
							tf_mixedNBNumber.setText("");
							tf_mixedNBNumber.requestFocusInWindow();
						}
					}else{
						int nb = Integer.parseInt(tf_mixedNBNumber.getText());
						if(nb >= 8){
							lb_info.setText(lang.Messages.getString("nb_smaller_than_eight"));
							tf_mixedNBNumber.setText("");
						}else{
							mixedNBNumber = nb;
							tf_mixedNBNumber.setEditable(false);
							tf_mixedNBNumerator.setEditable(true);
							tf_mixedNBNumerator.requestFocusInWindow();
						}
					}
				}
			}else if(obj == tf_mixedNBNumerator & key == KeyEvent.VK_ENTER){
				if(readInputInt(tf_mixedNBNumerator) != null){
					int num = readInputInt(tf_mixedNBNumerator);
					if(bl_randomProblem){
						if(num == mixedNBNumerator){
							tf_mixedNBNumerator.setEditable(false);
							tf_mixedNBDenominator.setEditable(true);
							tf_mixedNBDenominator.requestFocusInWindow();
						}else{
							tf_mixedNBNumerator.setText("");
							tf_mixedNBNumerator.requestFocusInWindow();
						}
					}else{
						mixedNBNumerator = readInputInt(tf_mixedNBNumerator);
						tf_mixedNBNumerator.setEditable(false);
						tf_mixedNBDenominator.setEditable(true);
						tf_mixedNBDenominator.requestFocusInWindow();
					}
				}
			}else if(obj == tf_mixedNBDenominator & key == KeyEvent.VK_ENTER){
				if(readInputInt(tf_mixedNBDenominator) != null){
					int pn = Integer.parseInt(tf_mixedNBDenominator.getText());
					if(bl_randomProblem){
						if(pn == improperFractionDenominator){
							tf_mixedNBDenominator.setEditable(false);
							tf_improperFractionNumerator.setEditable(true);
							tf_improperFractionNumerator.requestFocusInWindow();
						}else{
							tf_mixedNBDenominator.setText("");
							tf_mixedNBDenominator.requestFocusInWindow();
						}
					}else{	
						if(pn <= mixedNBNumerator){
							lb_info.setText(lang.Messages.getString("dn_must_be_bigger"));
							tf_mixedNBDenominator.setText("");
						}else{
							improperFractionDenominator = pn;
							improperFractionNumerator = mixedNBNumber * improperFractionDenominator + mixedNBNumerator;
							if(isVisible){drawPizzaBlech();}
							tf_improperFractionDenominator.setText(String.valueOf(improperFractionDenominator));
							tf_mixedNBDenominator.setEditable(false);
							tf_improperFractionNumerator.setEditable(true);
							tf_improperFractionNumerator.requestFocusInWindow();
						}
					}
				}
			}else if(obj == tf_improperFractionNumerator & key == KeyEvent.VK_ENTER){
				if(readInputInt(tf_improperFractionNumerator) != null){
					int num2 = Integer.parseInt(tf_improperFractionNumerator.getText());
					if( num2 != improperFractionNumerator ){
						lb_info.setText(lang.Messages.getString("how_many_pieces"));
						tf_improperFractionNumerator.setText("");
					}else{
						lb_info.setText(lang.Messages.getString("that_is_right"));
						if(bl_randomProblem){
							points++;
						}
						if(bl_randomProblem && !isVisible && was_neverVisible){
							//extrapunkt
							points++;
						}
						pdsp.setText(String.valueOf(points));
						owner.setPoints(points, "gemischteZahlen");
						tf_improperFractionNumerator.setEditable(false);
						drawPizzaBlech();
						btn_continue.requestFocusInWindow();
					}
				}
			}
		}//keyPressed
		public void keyTyped(KeyEvent event){}
		public void keyReleased(KeyEvent event){}
}//class
