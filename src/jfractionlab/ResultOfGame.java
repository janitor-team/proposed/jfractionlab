/**
 *	JFractionLab
 *	Copyright (C) 2005 jochen georges, gnugeo _ at _ gnugeo _ dot _ de
 *
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/

package jfractionlab;

import java.io.Serializable;
import java.util.Calendar;
import java.util.GregorianCalendar;


public class ResultOfGame implements Serializable{
	static final long serialVersionUID = JFractionLab.serialVersionUID;
		private String playersName;
		private String timeStamp;
		
		private int ptsCN;
		private int ptsNF;
		private int ptsCF;
		private int ptsEF;
		private int ptsRF;
		private int ptsIF;
		private int ptsMN;
		private int ptsFtD;
		private int ptsDtF;
		private int ptsPL;
		private int ptsMIN;
		private int ptsMF;
		private int ptsDFN;
		private int ptsDNF;
		private int ptsDFF;
		
		private GregorianCalendar cal = new GregorianCalendar();
		
		/**
		 * 
		 *
		 */
		public ResultOfGame(){}
		
		/**
		 * 
		 * @param sn
		 * @param ptsCN
		 * @param ptsNF
		 * @param ptsCF
		 * @param ptsEF
		 * @param ptsRF
		 * @param ptsIF
		 * @param ptsMN
		 * @param ptsPL
		 * @param ptsMIN
		 * @param ptsMF
		 * @param ptsDFN
		 * @param ptsDNF
		 * @param ptsDFF
		 * @param p_14
		 */
		public ResultOfGame(String sn,int ptsCN,int ptsNF,int ptsCF,int ptsEF,int ptsRF,int ptsIF,int ptsMN,int ptsFtD, int ptsDtF,
				int ptsPL,int ptsMIN,int ptsMF,int ptsDFN,int ptsDNF,int ptsDFF){
			setAll(sn,ptsCN,ptsNF,ptsCF,ptsEF,ptsRF,ptsIF,ptsMN,ptsFtD,ptsDtF,ptsPL,ptsMIN,ptsMF,ptsDFN,ptsDNF,ptsDFF);
		}

		/**
		 * 
		 * @param sn
		 * @param ptsCN
		 * @param ptsNF
		 * @param ptsCF
		 * @param ptsEF
		 * @param ptsRF
		 * @param ptsIF
		 * @param ptsMN
		 * @param ptsPL
		 * @param ptsMIN
		 * @param ptsMF
		 * @param ptsDFN
		 * @param ptsDNF
		 * @param ptsDFF
		 * @param p_14
		 */
		public void setAll(String sn,int ptsCN,int ptsNF,int ptsCF,int ptsEF,int ptsRF,int ptsIF,int ptsMN,int ptsFtD,int ptsDtF,int ptsPL,int ptsMIN,int ptsMF,int ptsDFN,int ptsDNF,int ptsDFF){
			this.playersName = sn;
			this.ptsCN = ptsCN;
			this.ptsNF = ptsNF;
			this.ptsCF = ptsCF;
			this.ptsEF = ptsEF;
			this.ptsRF = ptsRF;
			this.ptsIF = ptsIF;
			this.ptsMN = ptsMN;
			this.ptsFtD = ptsFtD;
			this.ptsDtF = ptsDtF;
			this.ptsPL = ptsPL;
			this.ptsMIN = ptsMIN;
			this.ptsMF = ptsMF;
			this.ptsDFN = ptsDFN;
			this.ptsDNF = ptsDNF;
			this.ptsDFF = ptsDFF;
			timeStamp = 
				cal.get(Calendar.YEAR)+"."
				+(cal.get(Calendar.MONTH)+1)+"."
				+cal.get(Calendar.DATE)+"_"
				+cal.get(Calendar.HOUR_OF_DAY)+"."
				+cal.get(Calendar.MINUTE);
			//showAll();
		}	
		
		/**
		 * 
		 * @param sn
		 */
		public void setPlayerName(String sn){
			this.playersName = sn;
		}
		
		/**
		 * 
		 * @return
		 */
		public String getPlayerName(){
			return playersName;
		}
		
		/**
		 * 
		 * @return
		 */
		public String getTimeStamp(){
			return timeStamp;
		}
		
		/**
		 * 
		 * @return
		 */
		public int getPtsCN(){return ptsCN;}
		public int getPtsNF(){return ptsNF;}
		public int getPtsCF(){return ptsCF;}
		public int getPtsEF(){return ptsEF;}
		public int getPtsRF(){return ptsRF;}
		public int getPtsIF(){return ptsIF;}
		public int getPtsMN(){return ptsMN;}
		public int getPtsFtD(){return ptsFtD;}
		public int getPtsDtF(){return ptsDtF;}
		public int getPtsPL(){return ptsPL;}
		public int getPtsMIN(){return ptsMIN;}
		public int getPtsMF(){return ptsMF;}
		public int getPtsDFN(){return ptsDFN;}
		public int getPtsDNF(){return ptsDNF;}
		public int getPtsDFF(){return ptsDFF;}
		
		
		/**
		 * 
		 *
		 */
		public void showAll(){
			System.out.println("");
			System.out.println("######################");
			System.out.println("ResultOfGame : ");
			System.out.println("######################");
			System.out.println("timestamp             :  "+ timeStamp);
			System.out.println("players name          :  "+ playersName);
			System.out.println("points 01             :  "+ String.valueOf(ptsCN));
			System.out.println("points 02             :  "+ String.valueOf(ptsNF));
			System.out.println("points 03             :  "+ String.valueOf(ptsCF));
			System.out.println("points 04             :  "+ String.valueOf(ptsEF));
			System.out.println("points 05             :  "+ String.valueOf(ptsRF));
			System.out.println("points 06             :  "+ String.valueOf(ptsIF));
			System.out.println("points 07             :  "+ String.valueOf(ptsMN));
			System.out.println("points 08             :  "+ String.valueOf(ptsFtD));
			System.out.println("points 09             :  "+ String.valueOf(ptsDtF));
			System.out.println("points 10             :  "+ String.valueOf(ptsPL));
			System.out.println("points 11             :  "+ String.valueOf(ptsMIN));
			System.out.println("points 12             :  "+ String.valueOf(ptsMF));
			System.out.println("points 13             :  "+ String.valueOf(ptsDFN));
			System.out.println("points 14             :  "+ String.valueOf(ptsDNF));
			System.out.println("points 15             :  "+ String.valueOf(ptsDFF));
			System.out.println("######################");
			System.out.println("");
		}
	}//FrageArray