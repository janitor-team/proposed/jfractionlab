/**
 *	JFractionLab
 *	Copyright (C) 2005 jochen georges, gnugeo _ at _ gnugeo _ dot _ de
 *
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/

package jfractionlab.displays;

import java.awt.Color;
import java.awt.Font;

import javax.swing.JLabel;
import javax.swing.border.CompoundBorder;
import javax.swing.border.LineBorder;

import jfractionlab.JFractionLab;

public class PointDisplay extends JLabel {
	static final long serialVersionUID = JFractionLab.serialVersionUID;
	
	public PointDisplay(){
		super();
		setPreferences();
	}//PointDisplay()
	
	/**
	 * 
	 * @param withBorder
	 */
	public PointDisplay(boolean withBorder){
		super();
		if (withBorder){
			setBorder(
				new CompoundBorder(
					new LineBorder(Color.WHITE, 1),
					new LineBorder(Color.BLACK, 1)
				)
			);
		}//if
		setPreferences();
	}//PointDisplay(boolean withBorder)
	
	/**
	 * 
	 *
	 */
	public void setPreferences(){
		setFont(new Font("sanserif", Font.BOLD, 24));
		setOpaque(true);
		setBackground(Color.WHITE);
		setHorizontalAlignment(JLabel.CENTER);
		setText("0");
	}//setPreferences
}
