package jfractionlab.exerciseGenerator;

import jfractionlab.FractionMaker;

public class GeneratorOfMixedNumbersExercises extends GeneratorObject {

	@Override
	public Exercises getOneExercise(
			int fullNumbersMax,
			int fractionsMax
	){
		Exercises ex = new Exercises("");
		int n1;
		int d1;
		int fullNB;
		String strExercise;
		String strQuickSolution;
		String strLongSolution;
		
		//generate numbers
		FractionMaker fraction = new FractionMaker();
		fraction.mkOneReducedFraction(fractionsMax);
		n1 = fraction.getNumerator_1();
		d1 = fraction.getDenominator_1();
		fullNB = Math.abs(jfractionlab.JFractionLab.ran.nextInt(fullNumbersMax))+1;
		
		//generate text of exercise and add it to the arraylist
		strExercise = fullNB+"{"+n1+"}over{"+d1+"}";
		ex.getAlExercises().add(strExercise+" ={}");

		//generate text of quicksolution and add it to the arraylist
		strQuickSolution = "{"+(fullNB*d1+n1)+"}over{"+d1+"}";
		ex.getAlSolutions().add(strQuickSolution);

		//generate text of longsolution and add it to the arraylist
		strLongSolution = "{"+fullNB+" cdot "+d1+" + "+n1+"}over{"+d1+"}";
		ex.getAlCalculations().add(
				strExercise+
				" = "+
				strLongSolution+
				" = "+
				strQuickSolution
		);
		return ex;
	}
}
