package jfractionlab.exerciseGenerator;

import jfractionlab.FractionMaker;

public class GeneratorOfMultiplicationExercises extends GeneratorObject {

	@Override
	public Exercises getOneExercise(
			int max1,
			int max2
	){
		Exercises ex = new Exercises("");
		int n1;
		int d1;
		int n2;
		int d2;
		String strExercise;
		String strQuickSolution;
		String strLongSolution;
			
		//generate numbers
		FractionMaker fraction = new FractionMaker(max1);
		n1 = fraction.getNumerator_1();
		d1 = fraction.getDenominator_1();
		n2 = fraction.getNumerator_2();
		d2 = fraction.getDenominator_2();
		
		//testnumbers
//		n1=5; d1=7; n2=3; d2=8;
//		n1=7; d1=5; n2=8; d2=3;
//		n1=4; d1=5; n2=7; d2=8;		//n1 with d2
//		n1=5; d1=8; n2=6; d2=7;		//d1 with n2
//		n1=4; d1=8; n2=5; d2=9;		//n1 with d1
//		n1=5; d1=9; n2=4; d2=8;		//n2 with d2
//		n1=6; d1=9; n2=5; d2=6;		//n1 with d1 and d2; n1~d2 is stronger
//		n1=6; d1=9; n2=6; d2=9;
//		n1=; d1=; n2=; d2=;
//		n1=; d1=; n2=; d2=;
//		n1=; d1=; n2=; d2=;
//		n1=; d1=; n2=; d2=;
//		n1=; d1=; n2=; d2=;
//		n1=; d1=; n2=; d2=;
//		n1=; d1=; n2=; d2=;
		
		//generate text of exercise and add it to the arraylist
		strExercise = "{{"+n1+"}over{"+d1+"}} cdot {{"+n2+"}over{"+d2+"}}";
		ex.getAlExercises().add(strExercise+" ={}");
		
		//generate text of quicksolution and add it to the arraylist
		strQuickSolution = writeReducingTextMultiplyDivide(false, n1, d1, n2, d2);
		ex.getAlSolutions().add(strQuickSolution);

		//generate text of longsolution and add it to the arraylist
		strLongSolution = writeReducingTextMultiplyDivide(true, n1, d1, n2, d2);
		if(strLongSolution.equals("")){
			ex.getAlCalculations().add(
					strExercise+
					" = "+
					strQuickSolution
			);
		}else{
			ex.getAlCalculations().add(
					strExercise+
					" = "+
					strLongSolution+
					" = "+
					strQuickSolution
			);
		}
		return ex;
	}
}
