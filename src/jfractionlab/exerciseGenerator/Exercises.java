package jfractionlab.exerciseGenerator;

import java.util.ArrayList;

public class Exercises {

	private String title;
	private ArrayList<String> alExercises;
	private ArrayList<String> alSolutions;
	private ArrayList<String> alCalculations;
	
	public Exercises(String title){
		this.title = title;
		alExercises = new ArrayList<String>();
		alSolutions = new ArrayList<String>();
		alCalculations  = new ArrayList<String>();
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}
	
	public ArrayList<String> getAlExercises() {
		return alExercises;
	}

	public void setAlExercises(ArrayList<String> alExercises) {
		this.alExercises = alExercises;
	}

	public ArrayList<String> getAlSolutions() {
		return alSolutions;
	}

	public void setAlSolutions(ArrayList<String> alQuicksolutions) {
		this.alSolutions = alQuicksolutions;
	}

	public ArrayList<String> getAlCalculations() {
		return alCalculations;
	}

	public void setAlCalculations(ArrayList<String> alLongSolutions) {
		this.alCalculations = alLongSolutions;
	}
}
