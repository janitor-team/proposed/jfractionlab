/**
 *	JFractionLab
 *	Copyright (C) 2005 jochen georges, gnugeo _ at _ gnugeo _ dot _ de
 *
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/

package jfractionlab.jflDialogs;

import info.clearthought.layout.TableLayout;

import java.awt.Color;
import java.awt.Container;
import java.awt.Dialog;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.filechooser.FileFilter;
import javax.swing.filechooser.FileNameExtensionFilter;

import jfractionlab.JFractionLab;
import jfractionlab.ResultOfGame;

public class LogSaverDialog extends JDialog implements ActionListener{
	static final long serialVersionUID = JFractionLab.serialVersionUID;
		private String playersName;
		private int ptsCN;
		private int ptsNF;
		private int ptsCF;
		private int ptsEF;
		private int ptsRF;
		private int ptsIF;
		private int ptsMN;
		private int ptsFtD;
		private int ptsDtF;
		private int ptsPL;
		private int ptsMIN;
		private int ptsMF;
		private int ptsDFN;
		private int ptsDNF;
		private int ptsDFF;
		
		private JLabel lb_txt = new JLabel("", JLabel.CENTER);
		private JLabel lb_name = new JLabel("", JLabel.CENTER);
		private JTextField tf_name = new JTextField(30);//MyJTextField(30);
		private JButton btn_reduce = new JButton("");
		private JButton btn_OK = new JButton("");
		public boolean isSavedSuccessfully = false;
	public LogSaverDialog(
			int ptsCN,int ptsNF,int ptsCF,int ptsEF,int ptsRF,int ptsIF,
			int ptsMN, int ptsFtD,int ptsDtF,
			int ptsPL,int ptsMIN,int ptsMF,int ptsDFN,int ptsDNF,int ptsDFF
		){ //neues Spiel machen
		setTitle(lang.Messages.getString("save_results"));
		setModalityType(Dialog.ModalityType.APPLICATION_MODAL);
		setLocale(lang.Messages.getLocale());
		lb_txt.setText(lang.Messages.getString("save_results"));
		lb_name.setText(lang.Messages.getString("your_name"));
		btn_reduce.setText(lang.Messages.getString("cancel"));
		btn_OK.setText(lang.Messages.getString("OK"));
		//this.playersName = sn;
		this.ptsCN = ptsCN;
		this.ptsNF = ptsNF;
		this.ptsCF = ptsCF;
		this.ptsEF = ptsEF;
		this.ptsRF = ptsRF;
		this.ptsIF = ptsIF;
		this.ptsMN = ptsMN;
		this.ptsFtD = ptsFtD;
		this.ptsDtF= ptsDtF;
		this.ptsPL = ptsPL;
		this.ptsMIN = ptsMIN;
		this.ptsMF = ptsMF;
		this.ptsDFN = ptsDFN;
		this.ptsDNF = ptsDNF;
		this.ptsDFF = ptsDFF;
		
		setLocation(150, 150);
		setSize(250,150);
		//Groessenveraenderung verhindern!
		setResizable(false);
		setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		makeGUI();
	}//Konstruktor

		/**
		 * 
		 *
		 */
	private void makeGUI(){
		double sizes[][] = {{0.5, 0.5},{0.33,0.33,0.33}}; //Spalten / Zeilen
		getRootPane().setDefaultButton(btn_OK);
		Container content = getContentPane();
		content.setLayout(new TableLayout(sizes));
		content.setBackground(Color.white);
		content.add(lb_txt, "0,0,1,0");
		content.add(lb_name, "0,1");
		content.add(tf_name, "1,1");
		btn_reduce.addActionListener(this);
		content.add(btn_reduce, "0,2");
		btn_OK.addActionListener(this);
		content.add(btn_OK, "1,2");
		setVisible(true);
		tf_name.requestFocusInWindow();
	}//makeGUI

	/**
	 *
	 */
	public void okAction(){
		if(!tf_name.getText().equals("")){
			playersName = tf_name.getText();
			ResultOfGame sl = new ResultOfGame(
					playersName,ptsCN,ptsNF,ptsCF,ptsEF,ptsRF,ptsIF,ptsMN, ptsFtD,ptsDtF,
					ptsPL,ptsMIN,ptsMF,ptsDFN,ptsDNF,ptsDFF);
			String zeitstempel = sl.getTimeStamp();
			JFileChooser fc = new JFileChooser();
			FileFilter myfilter = new FileNameExtensionFilter("jfl", "JFL");
			fc.addChoosableFileFilter(myfilter);
			fc.setLocale(lang.Messages.getLocale());
			fc.setSelectedFile(new File("JFractionLab_"+playersName+"_"+zeitstempel+".jfl"));
			int returnVal = fc.showSaveDialog(LogSaverDialog.this);
//			int returnVal = fc.showSaveDialog(fc);
			if (returnVal == JFileChooser.APPROVE_OPTION) {
				String str = fc.getSelectedFile().toString();
				if(!str.toLowerCase().endsWith(".jfl")){
					str += ".jfl";
				}
				try{
					FileOutputStream fs = new FileOutputStream(new File(str));
					ObjectOutputStream os = new ObjectOutputStream(fs);
					os.writeObject(sl);
					os.close();
					isSavedSuccessfully = true;
				}catch (IOException ex) {
					isSavedSuccessfully = false;
					ex.printStackTrace();
				}//catch
			}
			setVisible(false);
			dispose();
		}else{
			JOptionPane.showMessageDialog(null,
				lang.Messages.getString("insert_name"));
		}
		tf_name.requestFocusInWindow();
	}//okAction


	/**
	 * 
	 */
	@Override
	public void actionPerformed(ActionEvent e){
		Object obj = e.getSource();
	        if (obj == btn_reduce){
			setVisible(false);
		        dispose();
		}else if (obj == btn_OK ){
			okAction();
		}
	}//actionPerformed

}
