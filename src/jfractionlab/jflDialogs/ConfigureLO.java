/*
 * C:\Programme\LibreOffice3.4\URE\java
 * C:\Programme\LibreOffice3.4\Basis\program\classes
 * C:\Programme\LibreOffice3.4\program
 */

package jfractionlab.jflDialogs;

import info.clearthought.layout.TableLayout;

import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import jfractionlab.ConfManager;
import jfractionlab.JFractionLab;
import jfractionlab.officeMachine.OfficeStarter;

public class ConfigureLO extends JDialog implements ActionListener{
	static final long serialVersionUID = JFractionLab.serialVersionUID;
	
	private JLabel lb_intro = new JLabel("");
	
	private JPanel pn_outer = new JPanel();
	private JLabel lb_outer = new JLabel("");
	private JPanel pn_inner = new JPanel();
	
	private JPanel pn_jurt = new JPanel();
	private JLabel lb_jurt = new JLabel("", JLabel.LEFT);
	private JTextField jtf_jurt = new JTextField();
	private JButton btn_jurt = new JButton();
	
	private JPanel pn_unoil = new JPanel();
	private JLabel lb_unoil = new JLabel("", JLabel.LEFT);
	private JTextField jtf_unoil = new JTextField();
	private JButton btn_unoil = new JButton();
	
	private JPanel pn_soffice = new JPanel();
	private JLabel lb_soffice = new JLabel("", JLabel.LEFT);
	private JTextField jtf_soffice = new JTextField();
	private JButton btn_soffice = new JButton();
	
	private JButton btn_OK = new JButton("");
	private JButton btn_Cancel = new JButton("");
	
	public ConfigureLO() {
		makeGUI();
	}

	/**
	 * 
	 *
	 */
	private void makeGUI(){
		setLocation(50, 50);
		setSize(600, 650);
//		setResizable(false);
		setTitle(lang.Messages.getString("configure_libreoffice"));
		setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		double sizes_main[][] = {{
			5,
			TableLayout.FILL,
			TableLayout.FILL,
			5
		},{
			5,					//rand
			0.2,				//info
			TableLayout.FILL,	//pn_outer
			5,					//abstand
			30,					//buttons
			5					//rand
		}}; //Spalten / Zeilen
		double sizes_outer[][] = {{
			5,
			TableLayout.FILL,
			5
		},{
			5,
			0.45,
			TableLayout.FILL,
			5
		}}; //Spalten / Zeilen
		double sizes_inner[][] = {{
			5,
			TableLayout.FILL,
			5
		},{
			5,
			TableLayout.FILL,
			TableLayout.FILL,
			TableLayout.FILL,
			5
		}}; //Spalten / Zeilen
		getRootPane().setDefaultButton(btn_OK);
		Container content = getContentPane();
		content.setLayout(new TableLayout(sizes_main));
		
		pn_outer.setLayout(new TableLayout(sizes_outer));
		pn_outer.setBorder(BorderFactory.createEtchedBorder());
		pn_inner.setLayout(new TableLayout(sizes_inner));
		
		/* pn_main
		 * 		pn_outer
		 * 			pn_inner
		 * 				pn_jurt
		 * 				pn_unoil
		 * 				pn_soffice
		 * 
		 * build_all_components from inner to outer
		 */
		
		//build pn_inner
		lb_jurt.setText("jurt.jar");
		makePanel(pn_jurt, lb_jurt, jtf_jurt, btn_jurt);
		pn_inner.add(pn_jurt, "1,1");
		
		lb_unoil.setText("unoil.jar");
		makePanel(pn_unoil, lb_unoil, jtf_unoil, btn_unoil);
		pn_inner.add(pn_unoil, "1,2");
		
		lb_soffice.setText("soffice / soffice.exe");
		makePanel(pn_soffice, lb_soffice, jtf_soffice, btn_soffice);
		pn_inner.add(pn_soffice, "1,3");
		
		//build pn_outer
		lb_outer.setText("<html><p align=center>"+
				lang.Messages.getString("find_lo_jars")+"<br><br>"+
				lang.Messages.getString("example_path")+"<br>"+
				"Linux   : /opt/libreoffice3.4/ure/share/java/<br>"+
				"Windows : C:\\Programme\\LibreOffice3.4\\URE\\java</p></html>"
		);
		pn_outer.add(lb_outer, "1,1");
		pn_outer.add(pn_inner, "1,2");
		
		//build main
		lb_intro.setText("<html><p align=center>"+
				lang.Messages.getString(
						"officepath_needs_to_be_configured")+"<br>"+
				lang.Messages.getString("jfl_needs_three_paths")+"</p></html>"
		);
		content.add(lb_intro, "1,1,2,1");
		content.add(pn_outer, "1,2,2,2");
		
		btn_Cancel.addActionListener(this);
		btn_Cancel.setText(lang.Messages.getString("cancel"));
		content.add(btn_Cancel, "1,4");
		
		btn_OK.addActionListener(this);
		btn_OK.setText(lang.Messages.getString("OK"));
		content.add(btn_OK, "2,4");
		setVisible(true);
	}//makeGUI
	
	
	private void makePanel(JPanel pn, JLabel lb, JTextField jtf, JButton btn){
		double sizes_pn[][] = {{
			5,
			0.9,
			TableLayout.FILL,
			5
		},{
			5,
			TableLayout.FILL,
			30,
			5
		}}; //Spalten / Zeilen
		pn.setLayout(new TableLayout(sizes_pn));
		pn.setBorder(BorderFactory.createEtchedBorder());
		
		pn.add(lb, "1,1,2,1");
		//JOptionPane.showMessageDialog(this, new ConfManager().alertConfigurations());
		
		String[] ar = new ConfManager().getUnoConfJars();
		if(jtf == jtf_jurt){
			String str = ar[0];
			if(str.length()>0) jtf.setText(str.substring(0,str.lastIndexOf(File.separator)));
		}else if (jtf == jtf_unoil){
			String str = ar[(JFractionLab.nbOfUnoConfJars-2)];
			if(str.length()>0) jtf.setText(str.substring(0,str.lastIndexOf(File.separator)));
		}else if(jtf == jtf_soffice){
			String str = ar[(JFractionLab.nbOfUnoConfJars-1)];
			if(str.length()>0) jtf.setText(str.substring(0,str.lastIndexOf(File.separator)));
		}
		pn.add(jtf, "1,2");
		btn.addActionListener(this);
		btn.setText(" ... ");
		pn.add(btn, "2,2");
	}
	
	private void okAction(){
		boolean bl = true;
		String[] uno_jars = new String[JFractionLab.nbOfUnoConfJars];
		String malPath = "";
		
		String jurtPath= jtf_jurt.getText().trim();
		String unoilPath = jtf_unoil.getText().trim();
		String sofficePath = jtf_soffice.getText().trim();
		
		if(jurtPath.endsWith("jurt.jar")){
			jurtPath = jurtPath.substring(0, jurtPath.indexOf("jurt.jar"));
			jtf_jurt.setText(jurtPath);
		}
		if(unoilPath.endsWith("unoil.jar")){
			unoilPath = unoilPath.substring(0, unoilPath.indexOf("unoil.jar"));
			jtf_unoil.setText(unoilPath);
		}
		
		if(sofficePath.endsWith("soffice")){
			sofficePath = sofficePath.substring(0, sofficePath.indexOf("soffice"));
			jtf_soffice.setText(sofficePath);
		}
		if(sofficePath.endsWith("soffice.exe")){
			sofficePath = sofficePath.substring(0, sofficePath.indexOf("soffice.exe"));
			jtf_soffice.setText(sofficePath);
		}
		
		String str = createURIString(jurtPath, "java_uno.jar");
		if(new File(str).exists()){
			uno_jars[0] = str;
		}else{
			bl = false;
			malPath=jurtPath;
		}
		str = createURIString(jurtPath, "juh.jar");
		if(new File(str).exists()){
			uno_jars[1] = str;
		}else{
			bl = false;
			malPath=jurtPath;
		}
		str = createURIString(jurtPath, "jurt.jar");
		if(new File(str).exists()){
			uno_jars[2] = str;
		}else{
			bl = false;
			malPath=jurtPath;
		}
		str = createURIString(jurtPath, "ridl.jar");
		if(new File(str).exists()){
			uno_jars[3] = str;
		}else{
			bl = false;
			malPath=jurtPath;
		}
		str = createURIString(jurtPath, "unoloader.jar");
		if(new File(str).exists()){
			uno_jars[4] = str;
		}else{
			bl = false;
			malPath=jurtPath;
		}
		str = createURIString(unoilPath, "unoil.jar");
		if(new File(str).exists()){
			uno_jars[5] = str;
		}else{
			bl = false;
			malPath=unoilPath;
		}
		
		if (JFractionLab.os.contains("win")){	
			str = createURIString(sofficePath, "soffice.exe");
			if(new File(str).exists()){
				uno_jars[6] = str;
			}else{
				bl = false;
				malPath=sofficePath;
			}
		}else{
			str = createURIString(sofficePath, "soffice");
			if(new File(str).exists()){
				uno_jars[6] = str;
			}else{
				bl = false;
				malPath=sofficePath;
			}
		}
		if(!bl){
			JOptionPane.showMessageDialog(this,
					lang.Messages.getString("cant_find_jar")+" : "+
					malPath
			);
		}else{
			ConfManager cm = new ConfManager();
			cm.setUnoConfJars(uno_jars);
			for(int i = 0; i < uno_jars.length; i++){
				try {
					OfficeStarter.addToClassPath(new File(uno_jars[i]));
				} catch (IllegalArgumentException e) {
					e.printStackTrace();
				} catch (SecurityException e) {
					e.printStackTrace();
				} catch (IllegalAccessException e) {
					e.printStackTrace();
				} catch (InvocationTargetException e) {
					e.printStackTrace();
				} catch (NoSuchMethodException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			// conf auslesen
			//JOptionPane.showMessageDialog(this, cm.readConfigurations());
			close();
		}
	}

	private String createURIString(String path, String file){
		String str;
		if(path.substring(path.length()-1).equals(File.separator)){
			str = path+file;
		}else{
			str = path+File.separator+file;
		}
		return str;
	}

	public void actionPerformed(ActionEvent e){
		Object obj = e.getSource();
		if (obj == btn_OK){
			okAction();
		}else if(obj == btn_Cancel){
			close();
		}else if(obj == btn_jurt){
			jtf_jurt.setText(selectDir());
		}else if(obj == btn_unoil){
			jtf_unoil.setText(selectDir());
		}else if(obj == btn_soffice){
			jtf_soffice.setText(selectDir());
		}
	}//actionPerformed

	public String selectDir(){
		
		JFileChooser fc = new JFileChooser();
		//warum hat setLocale keine Wirkung?
		fc.setLocale(lang.Messages.getLocale());
		fc.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
		int returnVal = fc.showOpenDialog(this);
		if (returnVal == JFileChooser.APPROVE_OPTION) {
			return fc.getSelectedFile().toString();
		}else{
			return "";
		}
	}
	
	private void close(){
		setVisible(false);
		dispose();
	}

	}//class InfoDialog
