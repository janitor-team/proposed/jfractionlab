/**
 *	JFractionLab
 *	Copyright (C) 2005 jochen georges, gnugeo _ at _ gnugeo _ dot _ de
 *
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/

package jfractionlab.jflDialogs;

import info.clearthought.layout.TableLayout;

import java.awt.Color;
import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JEditorPane;
import javax.swing.JScrollPane;

import jfractionlab.JFractionLab;
import jfractionlab.exerciseDialogs.ReduceFraction;

public class ReduceHintDialog extends JDialog implements ActionListener{
	static final long serialVersionUID = JFractionLab.serialVersionUID;
		private JEditorPane jep_info = new JEditorPane();
		private JButton btn_OK = new JButton("");
		private int numerator;
		private int denominator;
		private ReduceFraction owner;
	/**
	 * 
	 */
	public ReduceHintDialog(ReduceFraction owner, int numerator, int denominator){
		setTitle(lang.Messages.getString("common_denominators"));
		setModal(true);
		this.owner = owner;
		this.numerator = numerator;
		this.denominator = denominator;
		setLocation(150, 150);
		setSize(500,200);
		setResizable(false);
		setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		makeGUI();
	}//Konstruktor

	/**
	 * 
	 *
	 */
	private void makeGUI(){
		double sizes[][] = {{
			10, TableLayout.FILL, 10
		},{
			10, TableLayout.FILL, 50,10
		}}; //Spalten / Zeilen

		getRootPane().setDefaultButton(btn_OK);
		Container content = getContentPane();
			content.setLayout(new TableLayout(sizes));
		content.setBackground(Color.white);
		
		jep_info.setContentType("text/html");
			String howtotxt = JFractionLab.jep_fontface + "<b>"
				//+ lang.Messages.getString("gnu_gpl")+"<br>"
				+lang.Messages.getString("denominators")
				+" "
				+numerator
				+" = "
				+getDivisors(numerator)
				+"<br>"
				+"<br>"
				+lang.Messages.getString("denominators")
				+" "
				+denominator
				+" = "
				+getDivisors(denominator)
				+"<br>"
			;//String
			jep_info.setText(howtotxt);
			jep_info.setEditable(false);
			jep_info.setFocusable(false);
		content.add(new JScrollPane(jep_info), "1,1");
		
		btn_OK.addActionListener(this);
		btn_OK.setText(lang.Messages.getString("OK"));
		content.add(btn_OK, "1,2,c,c");
		setVisible(true);
	}//makeGUI
	
	private String getDivisors(int nb){
		String prov="{ 1";
		for (int i = 2; i < nb+1; i++){
			if(nb % i == 0){
				prov += "; "+String.valueOf(i);
			}
		}
		prov += "}";
		return prov;
	}
	
	private void close(){
		setVisible(false);
		owner.tf_denominator_2.setText("");
		owner.tf_denominator_2.setEditable(false);
		owner.tf_numerator_2.setText("");
		owner.tf_numerator_2.setEditable(true);
		owner.tf_numerator_2.requestFocusInWindow();
		dispose();
	}
	
	@Override
	public void actionPerformed(ActionEvent e){
		Object obj = e.getSource();
	        if (obj == btn_OK){
	        	close();
		}
	}//actionPerformed
	
	}//class InfoDialog