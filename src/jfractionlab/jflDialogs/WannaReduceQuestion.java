package jfractionlab.jflDialogs;

import info.clearthought.layout.TableLayout;

import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dialog;
import java.awt.FocusTraversalPolicy;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JDialog;
import javax.swing.JEditorPane;
import javax.swing.JFrame;
import javax.swing.JScrollPane;

import jfractionlab.JFractionLab;
import jfractionlab.exerciseDialogs.ExerciseDialog;

public class WannaReduceQuestion extends JDialog implements ActionListener{
	
	static final long serialVersionUID = JFractionLab.serialVersionUID;
	private Container content;
	private JEditorPane jep_text = new JEditorPane();
	private JButton btn_reduce_yes = new JButton("");
	private JButton btn_reduce_no = new JButton("");
	public ExerciseDialog owner;
	private JCheckBox cb_reduce_always = new JCheckBox();
	MyOwnFocusTraversalPolicy newPolicy;
	
	public WannaReduceQuestion(ExerciseDialog owner){
		this.owner = owner;
		setModalityType(Dialog.ModalityType.APPLICATION_MODAL);
		newPolicy = new MyOwnFocusTraversalPolicy();
		createGUI();
	}
	
	private void createGUI(){
		double sizes[][] = {{
			5,
			TableLayout.FILL,
			5,
			TableLayout.FILL,
			5
		},{
			5,
			TableLayout.FILL,
			0.25,
			0.25,
			5
		}}; //Spalten / Zeilen
		getRootPane().setDefaultButton(btn_reduce_yes);
		content = getContentPane();
		content.setLayout(new TableLayout(sizes));
		content.setBackground(Color.white);
			jep_text.setContentType("text/html");
			jep_text.setText(
					JFractionLab.jep_fontface
					+ "<b>"
					+ lang.Messages.getString("do_you_wanna_reduce")
					+ "</b></font>"
			);
			jep_text.setEditable(false);
		content.add(new JScrollPane(jep_text), "1,1,3,1");
			cb_reduce_always.setBackground(Color.white);
			cb_reduce_always.setText(lang.Messages.getString("reduce_always"));
			cb_reduce_always.addActionListener(this);
		content.add(cb_reduce_always, "1,2,3,2");
			btn_reduce_no.setText(lang.Messages.getString("no"));
			btn_reduce_no.addActionListener(this);
		content.add(btn_reduce_no, "1,3");
			btn_reduce_yes.setText(lang.Messages.getString("yes"));
			btn_reduce_yes.addActionListener(this);
		content.add(btn_reduce_yes, "3,3");

		setFocusTraversalPolicy(newPolicy);
		setLocation(150, 150);
		setSize(350,150);
		setResizable(true);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setVisible(true);	
	}//showUsabilityDialog
	
	private void actionYES(){
		if(cb_reduce_always.isSelected() ){owner.bl_with_reducing = true;}
		owner.bl_wannaReduceQuestion_AnswerIsYes = true;
		owner.rb_reducing.setSelected(true);
		close();
	}
	
	private void actionNO(){
		if(cb_reduce_always.isSelected() ){owner.bl_with_reducing = true;}
		close();
	}
	
	private void close(){
		setVisible(false);
		dispose();
	}
	@Override
	public void actionPerformed(ActionEvent e) {
		Object obj = e.getSource();
		if (obj == btn_reduce_no){
			actionNO();
		}else if (obj == btn_reduce_yes){
			actionYES();
		}else if(obj == cb_reduce_always){
			if(cb_reduce_always.isSelected()){
				actionYES();
			}
		}
	}
	
	public class MyOwnFocusTraversalPolicy extends FocusTraversalPolicy {
		@Override
		public Component getComponentAfter(Container aContainer,Component aComponent) {
			//soll: btn_yes, btn_no, cb
			if(aComponent.equals(btn_reduce_yes)) {
				return btn_reduce_no;
			}else if(aComponent.equals(btn_reduce_no)){
				return cb_reduce_always;
			}else if(aComponent.equals(cb_reduce_always)){
				return btn_reduce_yes;
			}else{
				return btn_reduce_yes;
			}
		}

		@Override
		public Component getComponentBefore(Container aContainer,Component aComponent) {
			//soll: btn_yes, cb, btn_no
			if(aComponent.equals(btn_reduce_yes)) {
				return cb_reduce_always;
			}else if(aComponent.equals(cb_reduce_always)){
				return btn_reduce_no;
			}else if(aComponent.equals(btn_reduce_no)){
				return btn_reduce_yes;
			}else{
				return btn_reduce_yes;
			}
		}

		@Override
		public Component getDefaultComponent(Container aContainer) {
			return btn_reduce_yes;
		}

		@Override
		public Component getFirstComponent(Container aContainer) {
			return btn_reduce_yes;
		}

		@Override
		public Component getLastComponent(Container aContainer) {return null;}
	}//MyOwnFocusTraversalPolicy
}
